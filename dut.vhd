-- entity to instantiate all 3 different architectures for 12bit counter
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A4

library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;



entity dut is
    port (
        err     : out std_logic;                        -- ERRor : invalid counter value
        cnt     : out std_logic_vector( 11 downto 0);   -- CouNT
        --
        valLd   : in  std_logic_vector( 11 downto 0);   -- init VALue in case of LoaD
        nLd     : in  std_logic;                        -- Not LoaD; low actve LoaD
        nClr    : in  std_logic;                        -- Not CLeaR : low actve CLeaR
        up      : in  std_logic;                        -- UP count command
        down    : in  std_logic;                        -- DOWN count command
        --
        clk     : in  std_logic;                        -- CLocK
        nres    : in  std_logic                         -- Not RESet ; low active synchronous reset
    );
end entity dut;



architecture arc of dut is
    component counter is
        port(
            err     : out std_logic;
            cnt     : out std_logic_vector( 11 downto 0);
            --
            valLd   : in  std_logic_vector( 11 downto 0);
            nLd     : in  std_logic;
			nClr    : in  std_logic;
            up      : in  std_logic;
            down    : in  std_logic;
            --
            clk     : in  std_logic;
            nres    : in  std_logic
        );
    end component counter;
    for all : counter use entity work.counter(arop);
    -- insert architecture you want to test in line above here
    -- argl arop aril

begin
    counteri : counter
        port map(
            err   => err,
            cnt   => cnt,
            --
            valLd => valLd,
            nLd   => nLd,
			nClr  => nClr,
            up    => up,
            down  => down,
            --
            clk   => clk,
            nres  => nres
        );
end architecture arc;