-- test bench for 12bit counter
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A4

library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;

entity tb is 
end entity tb;



architecture arc of tb is

    signal err     : std_logic;
    signal count   : std_logic_vector( 11 downto 0);
    --
    signal valLd   : std_logic_vector( 11 downto 0);
    signal nLd     : std_logic;
    signal nClr    : std_logic;
    signal up      : std_logic;
    signal down    : std_logic;
    --
    signal clk     : std_logic;
    signal nres    : std_logic;


    component DUT is
        port (
            err     : out std_logic;                        -- ERRor : invalid counter value
            cnt     : out std_logic_vector( 11 downto 0);   -- CouNT
            --
            valLd   : in  std_logic_vector( 11 downto 0);   -- init VALue in case of LoaD
            nLd     : in  std_logic;                        -- Not LoaD; low actve LoaD
            nClr    : in  std_logic;                        -- Not CLeaR : low actve CLeaR
            up      : in  std_logic;                        -- UP count command
            down    : in  std_logic;                        -- DOWN count command
            --
            clk     : in  std_logic;                        -- CLocK
            nres    : in  std_logic                         -- Not RESet ; low active synchronous reset
        );
    end component DUT;

    for all : DUT use entity work.dut(arc);


    component stimuliGen is
        port(
            nClr  : out std_logic;
            up    : out std_logic;
            down  : out std_logic;
            valLd : out std_logic_vector(11 downto 0);
            nLd   : out std_logic;
            --
            clk  : out std_logic;
            nres : out std_logic
        );
    end component stimuliGen;

    for all : stimuliGen use entity work.sg(arc);


begin
    duti : DUT
        port map(
            err     => err,
            cnt     => count,
            --
            up      => up,
            down    => down,
            valLd   => valLd,
            nLd     => nLd,
            nClr    => nClr,
            --
            clk     => clk,
            nres    => nres
        );
        
    sgi : stimuliGen
        port map(
            nClr  => nClr,
            up    => up,
            down  => down,
            valLd => valLd,
            nLd   => nLd,
            --
            clk   => clk,
            nres  => nres
        );

end architecture arc;