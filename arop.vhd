-- architecture arop - operator overloading - for 12bit counter
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A4



library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.std_logic_unsigned.all;

architecture arop of counter is
    signal err_ns          : std_logic;
    signal err_cs          : std_logic := '0';
    signal count_ns        : std_logic_vector(11 downto 0);
    signal count_cs        : std_logic_vector(11 downto 0) := (others=>'0');
    signal valLd_ns        : std_logic_vector(11 downto 0);
    signal valLd_cs        : std_logic_vector(11 downto 0) := (others=>'0');

    signal ff1st_nClr_cs   : std_logic := '1';
    signal ff2nd_nClr_cs   : std_logic := '1';
    signal Clr_cs          : std_logic := '1';

    signal ff1st_nLd_cs    : std_logic := '1';
    signal ff2nd_nLd_cs    : std_logic := '1';
    signal Ld_cs           : std_logic := '1';

    signal ff1st_updown_cs : std_logic_vector (1 downto 0) := "00";
    signal ff2nd_updown_cs : std_logic_vector (1 downto 0) := "00";
    signal updown_cs       : std_logic_vector (1 downto 0) := "00";
    
begin

   
    
    combiLogic:
    process (updown_cs, count_cs, Ld_cs, valLd_cs, err_cs, Clr_cs) is 
        variable count_v   : std_logic_vector(12 downto 0);
        variable result_v  : std_logic_vector(12 downto 0);
        variable operand_v : std_logic_vector(12 downto 0);
        variable updown_v  : std_logic_vector(1 downto 0);
        variable err_v     : std_logic;
    begin
        err_v     := err_cs;
        operand_v := (others => '0');
        count_v   := '0' & count_cs;
        updown_v  := updown_cs;
        
        if(Ld_cs = '1') then         -- load new count value
            result_v := valLd_cs(valLd_cs'high) & valLd_cs;
        else                        -- count:
            case updown_v is
                when "01" =>        -- down
                    operand_v := (others => '1');
                when "10" =>        -- up
                    operand_v    := (others => '0');
                    operand_v(0) := '1';
                when others =>        -- do nothing
                    null;
            end case;
            result_v := count_v + operand_v;
			if( result_v(result_v'high) /= count_v(count_v'high) ) then -- fail!
				err_v := '1';
			else
				err_v := '0' or err_v;
			end if;
        end if;

        if(Clr_cs = '1') then
            err_v := '0';
            -- result_v := (others => '0');
        end if;
        
        count_ns <= result_v(11 downto 0);
        err_ns   <= err_v;
    end process combiLogic;
    
    valLd_ns <= valLd;
    Ld_cs   <= not ff1st_nLd_cs  and ff2nd_nLd_cs;
    Clr_cs  <= not ff1st_nClr_cs and ff2nd_nClr_cs;

    -- concatination of up and down is: ff1st_updown_cs <= up & down;

    -- rising edge detection for 'down' signal
    updown_cs(0) <= ff1st_updown_cs(0) and not ff2nd_updown_cs(0);

    -- rising edge detection for 'up' signal
    updown_cs(1) <= ff1st_updown_cs(1) and not ff2nd_updown_cs(1); 
     
    sequLogic:
    process (clk) is
    begin
        if(clk = '1' and clk'event) then
            if(nres = '0') then
                err_cs          <= '0';
                count_cs        <= (others=>'0');
                valLd_cs        <= (others=>'0');
                ff1st_nClr_cs   <= '1';
                ff2nd_nClr_cs   <= '1';
                ff1st_nLd_cs    <= '1';
                ff2nd_nLd_cs    <= '1';
                ff1st_updown_cs <= "00";
                ff2nd_updown_cs <= "00";
            else
                err_cs          <= err_ns;
                count_cs        <= count_ns;
                valLd_cs        <= valLd_ns;
                ff1st_nClr_cs   <= nClr;
                ff2nd_nClr_cs   <= ff1st_nClr_cs;
                ff1st_nLd_cs    <= nLd;
                ff2nd_nLd_cs    <= ff1st_nLd_cs;
                ff1st_updown_cs <= up & down;
                ff2nd_updown_cs <= ff1st_updown_cs;
            end if;
        else
            null;
        end if;
    end process sequLogic;
    
    err <= err_cs;
    cnt <= count_cs;
    
end architecture arop;