onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb/err
add wave -noupdate -divider -height 50 Count
add wave -noupdate -radix decimal -childformat {{/tb/count(11) -radix decimal} {/tb/count(10) -radix decimal} {/tb/count(9) -radix decimal} {/tb/count(8) -radix decimal} {/tb/count(7) -radix decimal} {/tb/count(6) -radix decimal} {/tb/count(5) -radix decimal} {/tb/count(4) -radix decimal} {/tb/count(3) -radix decimal} {/tb/count(2) -radix decimal} {/tb/count(1) -radix decimal} {/tb/count(0) -radix decimal}} -expand -subitemconfig {/tb/count(11) {-height 15 -radix decimal} /tb/count(10) {-height 15 -radix decimal} /tb/count(9) {-height 15 -radix decimal} /tb/count(8) {-height 15 -radix decimal} /tb/count(7) {-height 15 -radix decimal} /tb/count(6) {-height 15 -radix decimal} /tb/count(5) {-height 15 -radix decimal} /tb/count(4) {-height 15 -radix decimal} /tb/count(3) {-height 15 -radix decimal} /tb/count(2) {-height 15 -radix decimal} /tb/count(1) {-height 15 -radix decimal} /tb/count(0) {-height 15 -radix decimal}} /tb/count
add wave -noupdate -divider -height 50 Inputs
add wave -noupdate -divider {value Load}
add wave -noupdate /tb/valLd
add wave -noupdate -divider {not Load}
add wave -noupdate /tb/nLd
add wave -noupdate -divider {not Clear}
add wave -noupdate /tb/nClr
add wave -noupdate -divider UP
add wave -noupdate /tb/up
add wave -noupdate -divider DOWN
add wave -noupdate /tb/down
add wave -noupdate -divider Clock
add wave -noupdate /tb/clk
add wave -noupdate -divider {not Reset}
add wave -noupdate /tb/nres
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1522658 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 234
configure wave -valuecolwidth 109
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {1722 ns}
