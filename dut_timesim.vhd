--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.49d
--  \   \         Application: netgen
--  /   /         Filename: dut_timesim.vhd
-- /___/   /\     Timestamp: Fri Jan 15 13:18:21 2016
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -rpw 100 -ar Structure -tm dut -w -dir netgen/fit -ofmt vhdl -sim dut.nga dut_timesim.vhd 
-- Device	: XC2C256-7-PQ208 (Speed File: Version 14.0 Advance Product Specification)
-- Input file	: dut.nga
-- Output file	: D:\DTP\A4\ise14x7_v1\iseWRK\netgen\fit\dut_timesim.vhd
-- # of Entities	: 1
-- Design Name	: dut.nga
-- Xilinx	: C:\Xilinx\14.4\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

entity dut is
  port (
    clk : in STD_LOGIC := 'X'; 
    nres : in STD_LOGIC := 'X'; 
    nLd : in STD_LOGIC := 'X'; 
    up : in STD_LOGIC := 'X'; 
    down : in STD_LOGIC := 'X'; 
    nClr : in STD_LOGIC := 'X'; 
    err : out STD_LOGIC; 
    valLd : in STD_LOGIC_VECTOR ( 11 downto 0 ); 
    cnt : out STD_LOGIC_VECTOR ( 11 downto 0 ) 
  );
end dut;

architecture Structure of dut is
  signal clk_II_FCLK_1 : STD_LOGIC; 
  signal nres_II_UIM_3 : STD_LOGIC; 
  signal nLd_II_UIM_5 : STD_LOGIC; 
  signal valLd_0_II_UIM_7 : STD_LOGIC; 
  signal up_II_UIM_9 : STD_LOGIC; 
  signal down_II_UIM_11 : STD_LOGIC; 
  signal valLd_10_II_UIM_13 : STD_LOGIC; 
  signal valLd_7_II_UIM_15 : STD_LOGIC; 
  signal valLd_1_II_UIM_17 : STD_LOGIC; 
  signal valLd_2_II_UIM_19 : STD_LOGIC; 
  signal valLd_3_II_UIM_21 : STD_LOGIC; 
  signal valLd_4_II_UIM_23 : STD_LOGIC; 
  signal valLd_5_II_UIM_25 : STD_LOGIC; 
  signal valLd_6_II_UIM_27 : STD_LOGIC; 
  signal valLd_8_II_UIM_29 : STD_LOGIC; 
  signal valLd_9_II_UIM_31 : STD_LOGIC; 
  signal valLd_11_II_UIM_33 : STD_LOGIC; 
  signal nClr_II_UIM_35 : STD_LOGIC; 
  signal cnt_0_MC_Q_37 : STD_LOGIC; 
  signal cnt_10_MC_Q_39 : STD_LOGIC; 
  signal cnt_11_MC_Q_41 : STD_LOGIC; 
  signal cnt_1_MC_Q_43 : STD_LOGIC; 
  signal cnt_2_MC_Q_45 : STD_LOGIC; 
  signal cnt_3_MC_Q_47 : STD_LOGIC; 
  signal cnt_4_MC_Q_49 : STD_LOGIC; 
  signal cnt_5_MC_Q_51 : STD_LOGIC; 
  signal cnt_6_MC_Q_53 : STD_LOGIC; 
  signal cnt_7_MC_Q_55 : STD_LOGIC; 
  signal cnt_8_MC_Q_57 : STD_LOGIC; 
  signal cnt_9_MC_Q_59 : STD_LOGIC; 
  signal err_MC_Q_61 : STD_LOGIC; 
  signal cnt_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_0_MC_UIM_63 : STD_LOGIC; 
  signal cnt_0_MC_D_64 : STD_LOGIC; 
  signal cnt_0_MC_tsimcreated_xor_Q_65 : STD_LOGIC; 
  signal Gnd_66 : STD_LOGIC; 
  signal Vcc_67 : STD_LOGIC; 
  signal cnt_0_MC_D1_68 : STD_LOGIC; 
  signal cnt_0_MC_D2_69 : STD_LOGIC; 
  signal N_PZ_139_70 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_0_71 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_1_72 : STD_LOGIC; 
  signal counteri_ff1st_nLd_cs_73 : STD_LOGIC; 
  signal counteri_ff2nd_nLd_cs_74 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_2_76 : STD_LOGIC; 
  signal N_PZ_162_79 : STD_LOGIC; 
  signal N_PZ_136_80 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_3_81 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_4_82 : STD_LOGIC; 
  signal counteri_ff1st_nLd_cs_MC_Q : STD_LOGIC; 
  signal counteri_ff1st_nLd_cs_MC_D_84 : STD_LOGIC; 
  signal counteri_ff1st_nLd_cs_MC_D1_85 : STD_LOGIC; 
  signal counteri_ff1st_nLd_cs_MC_D2_86 : STD_LOGIC; 
  signal counteri_ff2nd_nLd_cs_MC_Q : STD_LOGIC; 
  signal counteri_ff2nd_nLd_cs_MC_D_88 : STD_LOGIC; 
  signal counteri_ff2nd_nLd_cs_MC_D1_89 : STD_LOGIC; 
  signal counteri_ff2nd_nLd_cs_MC_D2_90 : STD_LOGIC; 
  signal counteri_valLd_cs_0_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_0_MC_D_92 : STD_LOGIC; 
  signal counteri_valLd_cs_0_MC_D1_93 : STD_LOGIC; 
  signal counteri_valLd_cs_0_MC_D2_94 : STD_LOGIC; 
  signal counteri_ff1st_updown_cs_1_MC_Q : STD_LOGIC; 
  signal counteri_ff1st_updown_cs_1_MC_D_96 : STD_LOGIC; 
  signal counteri_ff1st_updown_cs_1_MC_D1_97 : STD_LOGIC; 
  signal counteri_ff1st_updown_cs_1_MC_D2_98 : STD_LOGIC; 
  signal counteri_ff2nd_updown_cs_1_MC_Q : STD_LOGIC; 
  signal counteri_ff2nd_updown_cs_1_MC_D_100 : STD_LOGIC; 
  signal counteri_ff2nd_updown_cs_1_MC_D1_101 : STD_LOGIC; 
  signal counteri_ff2nd_updown_cs_1_MC_D2_102 : STD_LOGIC; 
  signal N_PZ_162_MC_Q_103 : STD_LOGIC; 
  signal N_PZ_162_MC_D_104 : STD_LOGIC; 
  signal N_PZ_162_MC_D1_105 : STD_LOGIC; 
  signal N_PZ_162_MC_D2_106 : STD_LOGIC; 
  signal counteri_ff1st_updown_cs_0_MC_Q : STD_LOGIC; 
  signal counteri_ff1st_updown_cs_0_MC_D_110 : STD_LOGIC; 
  signal counteri_ff1st_updown_cs_0_MC_D1_111 : STD_LOGIC; 
  signal counteri_ff1st_updown_cs_0_MC_D2_112 : STD_LOGIC; 
  signal counteri_ff2nd_updown_cs_0_MC_Q : STD_LOGIC; 
  signal counteri_ff2nd_updown_cs_0_MC_D_114 : STD_LOGIC; 
  signal counteri_ff2nd_updown_cs_0_MC_D1_115 : STD_LOGIC; 
  signal counteri_ff2nd_updown_cs_0_MC_D2_116 : STD_LOGIC; 
  signal N_PZ_136_MC_Q_117 : STD_LOGIC; 
  signal N_PZ_136_MC_D_118 : STD_LOGIC; 
  signal N_PZ_136_MC_D1_119 : STD_LOGIC; 
  signal N_PZ_136_MC_D2_120 : STD_LOGIC; 
  signal N_PZ_136_MC_D2_PT_0_121 : STD_LOGIC; 
  signal N_PZ_136_MC_D2_PT_1_122 : STD_LOGIC; 
  signal N_PZ_139_MC_Q_123 : STD_LOGIC; 
  signal N_PZ_139_MC_D_124 : STD_LOGIC; 
  signal N_PZ_139_MC_D1_125 : STD_LOGIC; 
  signal N_PZ_139_MC_D2_126 : STD_LOGIC; 
  signal N_PZ_139_MC_D2_PT_0_127 : STD_LOGIC; 
  signal N_PZ_139_MC_D2_PT_1_128 : STD_LOGIC; 
  signal cnt_10_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_10_MC_UIM_130 : STD_LOGIC; 
  signal cnt_10_MC_D_131 : STD_LOGIC; 
  signal cnt_10_MC_tsimcreated_xor_Q_132 : STD_LOGIC; 
  signal cnt_10_MC_D1_133 : STD_LOGIC; 
  signal cnt_10_MC_D2_134 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_0_135 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_1_137 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_2_138 : STD_LOGIC; 
  signal cnt_7_MC_UIM_139 : STD_LOGIC; 
  signal counteri_Madd_result_v_add0000_or0005_140 : STD_LOGIC; 
  signal cnt_8_MC_UIM_141 : STD_LOGIC; 
  signal cnt_9_MC_UIM_142 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_3_143 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_4_144 : STD_LOGIC; 
  signal counteri_valLd_cs_10_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_10_MC_D_146 : STD_LOGIC; 
  signal counteri_valLd_cs_10_MC_D1_147 : STD_LOGIC; 
  signal counteri_valLd_cs_10_MC_D2_148 : STD_LOGIC; 
  signal cnt_7_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_7_MC_D_150 : STD_LOGIC; 
  signal cnt_7_MC_tsimcreated_xor_Q_151 : STD_LOGIC; 
  signal cnt_7_MC_D1_152 : STD_LOGIC; 
  signal cnt_7_MC_D2_153 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_0_154 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_1_155 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_2_156 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_3_158 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_4_159 : STD_LOGIC; 
  signal counteri_valLd_cs_7_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_7_MC_D_161 : STD_LOGIC; 
  signal counteri_valLd_cs_7_MC_D1_162 : STD_LOGIC; 
  signal counteri_valLd_cs_7_MC_D2_163 : STD_LOGIC; 
  signal counteri_Madd_result_v_add0000_or0005_MC_Q_164 : STD_LOGIC; 
  signal counteri_Madd_result_v_add0000_or0005_MC_D_165 : STD_LOGIC; 
  signal counteri_Madd_result_v_add0000_or0005_MC_D1_166 : STD_LOGIC; 
  signal counteri_Madd_result_v_add0000_or0005_MC_D2_167 : STD_LOGIC; 
  signal counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_168 : STD_LOGIC; 
  signal cnt_1_MC_UIM_169 : STD_LOGIC; 
  signal cnt_2_MC_UIM_170 : STD_LOGIC; 
  signal cnt_3_MC_UIM_171 : STD_LOGIC; 
  signal cnt_4_MC_UIM_172 : STD_LOGIC; 
  signal cnt_5_MC_UIM_173 : STD_LOGIC; 
  signal cnt_6_MC_UIM_174 : STD_LOGIC; 
  signal counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_175 : STD_LOGIC; 
  signal counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_176 : STD_LOGIC; 
  signal cnt_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_1_MC_D_178 : STD_LOGIC; 
  signal cnt_1_MC_tsimcreated_xor_Q_179 : STD_LOGIC; 
  signal cnt_1_MC_D1_180 : STD_LOGIC; 
  signal cnt_1_MC_D2_181 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_0_182 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_1_183 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_2_185 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_3_186 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_4_187 : STD_LOGIC; 
  signal counteri_valLd_cs_1_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_1_MC_D_189 : STD_LOGIC; 
  signal counteri_valLd_cs_1_MC_D1_190 : STD_LOGIC; 
  signal counteri_valLd_cs_1_MC_D2_191 : STD_LOGIC; 
  signal cnt_2_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_2_MC_D_193 : STD_LOGIC; 
  signal cnt_2_MC_tsimcreated_xor_Q_194 : STD_LOGIC; 
  signal cnt_2_MC_D1_195 : STD_LOGIC; 
  signal cnt_2_MC_D2_196 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_0_197 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_1_198 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_2_200 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_3_201 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_4_202 : STD_LOGIC; 
  signal counteri_valLd_cs_2_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_2_MC_D_204 : STD_LOGIC; 
  signal counteri_valLd_cs_2_MC_D1_205 : STD_LOGIC; 
  signal counteri_valLd_cs_2_MC_D2_206 : STD_LOGIC; 
  signal cnt_3_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_3_MC_D_208 : STD_LOGIC; 
  signal cnt_3_MC_tsimcreated_xor_Q_209 : STD_LOGIC; 
  signal cnt_3_MC_D1_210 : STD_LOGIC; 
  signal cnt_3_MC_D2_211 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_0_212 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_1_213 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_2_215 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_3_216 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_4_217 : STD_LOGIC; 
  signal counteri_valLd_cs_3_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_3_MC_D_219 : STD_LOGIC; 
  signal counteri_valLd_cs_3_MC_D1_220 : STD_LOGIC; 
  signal counteri_valLd_cs_3_MC_D2_221 : STD_LOGIC; 
  signal cnt_4_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_4_MC_D_223 : STD_LOGIC; 
  signal cnt_4_MC_tsimcreated_xor_Q_224 : STD_LOGIC; 
  signal cnt_4_MC_D1_225 : STD_LOGIC; 
  signal cnt_4_MC_D2_226 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_0_227 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_1_229 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_2_230 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_3_231 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_4_232 : STD_LOGIC; 
  signal counteri_valLd_cs_4_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_4_MC_D_234 : STD_LOGIC; 
  signal counteri_valLd_cs_4_MC_D1_235 : STD_LOGIC; 
  signal counteri_valLd_cs_4_MC_D2_236 : STD_LOGIC; 
  signal cnt_5_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_5_MC_D_238 : STD_LOGIC; 
  signal cnt_5_MC_tsimcreated_xor_Q_239 : STD_LOGIC; 
  signal cnt_5_MC_D1_240 : STD_LOGIC; 
  signal cnt_5_MC_D2_241 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_0_242 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_1_244 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_2_245 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_3_246 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_4_247 : STD_LOGIC; 
  signal counteri_valLd_cs_5_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_5_MC_D_249 : STD_LOGIC; 
  signal counteri_valLd_cs_5_MC_D1_250 : STD_LOGIC; 
  signal counteri_valLd_cs_5_MC_D2_251 : STD_LOGIC; 
  signal cnt_6_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_6_MC_D_253 : STD_LOGIC; 
  signal cnt_6_MC_tsimcreated_xor_Q_254 : STD_LOGIC; 
  signal cnt_6_MC_D1_255 : STD_LOGIC; 
  signal cnt_6_MC_D2_256 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_0_257 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_1_259 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_2_260 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_3_261 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_4_262 : STD_LOGIC; 
  signal counteri_valLd_cs_6_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_6_MC_D_264 : STD_LOGIC; 
  signal counteri_valLd_cs_6_MC_D1_265 : STD_LOGIC; 
  signal counteri_valLd_cs_6_MC_D2_266 : STD_LOGIC; 
  signal cnt_8_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_8_MC_D_268 : STD_LOGIC; 
  signal cnt_8_MC_tsimcreated_xor_Q_269 : STD_LOGIC; 
  signal cnt_8_MC_D1_270 : STD_LOGIC; 
  signal cnt_8_MC_D2_271 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_0_272 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_1_273 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_2_275 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_3_276 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_4_277 : STD_LOGIC; 
  signal counteri_valLd_cs_8_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_8_MC_D_279 : STD_LOGIC; 
  signal counteri_valLd_cs_8_MC_D1_280 : STD_LOGIC; 
  signal counteri_valLd_cs_8_MC_D2_281 : STD_LOGIC; 
  signal cnt_9_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_9_MC_D_283 : STD_LOGIC; 
  signal cnt_9_MC_tsimcreated_xor_Q_284 : STD_LOGIC; 
  signal cnt_9_MC_D1_285 : STD_LOGIC; 
  signal cnt_9_MC_D2_286 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_0_287 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_1_289 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_2_290 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_3_291 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_4_292 : STD_LOGIC; 
  signal counteri_valLd_cs_9_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_9_MC_D_294 : STD_LOGIC; 
  signal counteri_valLd_cs_9_MC_D1_295 : STD_LOGIC; 
  signal counteri_valLd_cs_9_MC_D2_296 : STD_LOGIC; 
  signal cnt_11_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_11_MC_UIM_298 : STD_LOGIC; 
  signal cnt_11_MC_D_299 : STD_LOGIC; 
  signal cnt_11_MC_tsimcreated_xor_Q_300 : STD_LOGIC; 
  signal cnt_11_MC_D1_301 : STD_LOGIC; 
  signal cnt_11_MC_D2_302 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_0_303 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_1_305 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_2_306 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_3_307 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_4_308 : STD_LOGIC; 
  signal counteri_valLd_cs_11_MC_Q : STD_LOGIC; 
  signal counteri_valLd_cs_11_MC_D_310 : STD_LOGIC; 
  signal counteri_valLd_cs_11_MC_D1_311 : STD_LOGIC; 
  signal counteri_valLd_cs_11_MC_D2_312 : STD_LOGIC; 
  signal err_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal err_MC_UIM_314 : STD_LOGIC; 
  signal err_MC_D_315 : STD_LOGIC; 
  signal err_MC_D1_316 : STD_LOGIC; 
  signal err_MC_D2_317 : STD_LOGIC; 
  signal counteri_ff1st_nClr_cs_318 : STD_LOGIC; 
  signal err_MC_D2_PT_0_319 : STD_LOGIC; 
  signal counteri_ff2nd_nClr_cs_320 : STD_LOGIC; 
  signal err_MC_D2_PT_1_321 : STD_LOGIC; 
  signal err_MC_D2_PT_2_322 : STD_LOGIC; 
  signal err_MC_D2_PT_3_323 : STD_LOGIC; 
  signal err_MC_D2_PT_4_324 : STD_LOGIC; 
  signal err_MC_D2_PT_5_325 : STD_LOGIC; 
  signal counteri_ff1st_nClr_cs_MC_Q : STD_LOGIC; 
  signal counteri_ff1st_nClr_cs_MC_D_327 : STD_LOGIC; 
  signal counteri_ff1st_nClr_cs_MC_D1_328 : STD_LOGIC; 
  signal counteri_ff1st_nClr_cs_MC_D2_329 : STD_LOGIC; 
  signal counteri_ff2nd_nClr_cs_MC_Q : STD_LOGIC; 
  signal counteri_ff2nd_nClr_cs_MC_D_331 : STD_LOGIC; 
  signal counteri_ff2nd_nClr_cs_MC_D1_332 : STD_LOGIC; 
  signal counteri_ff2nd_nClr_cs_MC_D2_333 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_nLd_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_nLd_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_nLd_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_nLd_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_nLd_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_nLd_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_162_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_162_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_162_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_162_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_136_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_136_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_136_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_136_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_136_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_136_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_136_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_136_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_nClr_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_nClr_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_nClr_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_nClr_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_nClr_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff1st_nClr_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_ff1st_nLd_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_ff1st_nLd_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_ff2nd_nLd_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_ff2nd_nLd_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_162_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_136_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_139_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_5_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_ff1st_nClr_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_ff1st_nClr_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_ff2nd_nClr_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_counteri_ff2nd_nClr_cs_MC_D1_IN1 : STD_LOGIC; 
  signal counteri_valLd_cs : STD_LOGIC_VECTOR ( 11 downto 0 ); 
  signal counteri_ff1st_updown_cs : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal counteri_ff2nd_updown_cs : STD_LOGIC_VECTOR ( 1 downto 0 ); 
begin
  clk_II_FCLK : X_BUF
    port map (
      I => clk,
      O => clk_II_FCLK_1
    );
  nres_II_UIM : X_BUF
    port map (
      I => nres,
      O => nres_II_UIM_3
    );
  nLd_II_UIM : X_BUF
    port map (
      I => nLd,
      O => nLd_II_UIM_5
    );
  valLd_0_II_UIM : X_BUF
    port map (
      I => valLd(0),
      O => valLd_0_II_UIM_7
    );
  up_II_UIM : X_BUF
    port map (
      I => up,
      O => up_II_UIM_9
    );
  down_II_UIM : X_BUF
    port map (
      I => down,
      O => down_II_UIM_11
    );
  valLd_10_II_UIM : X_BUF
    port map (
      I => valLd(10),
      O => valLd_10_II_UIM_13
    );
  valLd_7_II_UIM : X_BUF
    port map (
      I => valLd(7),
      O => valLd_7_II_UIM_15
    );
  valLd_1_II_UIM : X_BUF
    port map (
      I => valLd(1),
      O => valLd_1_II_UIM_17
    );
  valLd_2_II_UIM : X_BUF
    port map (
      I => valLd(2),
      O => valLd_2_II_UIM_19
    );
  valLd_3_II_UIM : X_BUF
    port map (
      I => valLd(3),
      O => valLd_3_II_UIM_21
    );
  valLd_4_II_UIM : X_BUF
    port map (
      I => valLd(4),
      O => valLd_4_II_UIM_23
    );
  valLd_5_II_UIM : X_BUF
    port map (
      I => valLd(5),
      O => valLd_5_II_UIM_25
    );
  valLd_6_II_UIM : X_BUF
    port map (
      I => valLd(6),
      O => valLd_6_II_UIM_27
    );
  valLd_8_II_UIM : X_BUF
    port map (
      I => valLd(8),
      O => valLd_8_II_UIM_29
    );
  valLd_9_II_UIM : X_BUF
    port map (
      I => valLd(9),
      O => valLd_9_II_UIM_31
    );
  valLd_11_II_UIM : X_BUF
    port map (
      I => valLd(11),
      O => valLd_11_II_UIM_33
    );
  nClr_II_UIM : X_BUF
    port map (
      I => nClr,
      O => nClr_II_UIM_35
    );
  cnt_0_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_37,
      O => cnt(0)
    );
  cnt_10_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_39,
      O => cnt(10)
    );
  cnt_11_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_41,
      O => cnt(11)
    );
  cnt_1_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_43,
      O => cnt(1)
    );
  cnt_2_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_45,
      O => cnt(2)
    );
  cnt_3_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_47,
      O => cnt(3)
    );
  cnt_4_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_49,
      O => cnt(4)
    );
  cnt_5_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_51,
      O => cnt(5)
    );
  cnt_6_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_53,
      O => cnt(6)
    );
  cnt_7_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_55,
      O => cnt(7)
    );
  cnt_8_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_57,
      O => cnt(8)
    );
  cnt_9_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_59,
      O => cnt(9)
    );
  err_62 : X_BUF
    port map (
      I => err_MC_Q_61,
      O => err
    );
  cnt_0_MC_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_tsimrenamed_net_Q,
      O => cnt_0_MC_Q_37
    );
  cnt_0_MC_UIM : X_BUF
    port map (
      I => cnt_0_MC_Q_tsimrenamed_net_Q,
      O => cnt_0_MC_UIM_63
    );
  cnt_0_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_tsimcreated_xor_IN1,
      O => cnt_0_MC_tsimcreated_xor_Q_65
    );
  cnt_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_0_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_cnt_0_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => cnt_0_MC_Q_tsimrenamed_net_Q
    );
  Gnd : X_ZERO
    port map (
      O => Gnd_66
    );
  Vcc : X_ONE
    port map (
      O => Vcc_67
    );
  cnt_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D_IN1,
      O => cnt_0_MC_D_64
    );
  cnt_0_MC_D1 : X_ZERO
    port map (
      O => cnt_0_MC_D1_68
    );
  cnt_0_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1,
      O => cnt_0_MC_D2_PT_0_71
    );
  cnt_0_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1,
      O => cnt_0_MC_D2_PT_1_72
    );
  cnt_0_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_0_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_0_MC_D2_PT_2_IN3,
      O => cnt_0_MC_D2_PT_2_76
    );
  cnt_0_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_0_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_0_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_0_MC_D2_PT_3_IN3,
      O => cnt_0_MC_D2_PT_3_81
    );
  cnt_0_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_0_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_0_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN4,
      O => cnt_0_MC_D2_PT_4_82
    );
  cnt_0_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_0_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_0_MC_D2_IN4,
      O => cnt_0_MC_D2_69
    );
  counteri_ff1st_nLd_cs : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_MC_Q,
      O => counteri_ff1st_nLd_cs_73
    );
  counteri_ff1st_nLd_cs_MC_REG : X_FF
    generic map(
      INIT => '1'
    )
    port map (
      I => NlwBufferSignal_counteri_ff1st_nLd_cs_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_ff1st_nLd_cs_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_ff1st_nLd_cs_MC_Q
    );
  counteri_ff1st_nLd_cs_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_counteri_ff1st_nLd_cs_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_ff1st_nLd_cs_MC_D_IN1,
      O => counteri_ff1st_nLd_cs_MC_D_84
    );
  counteri_ff1st_nLd_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_ff1st_nLd_cs_MC_D1_IN0,
      I1 => NlwInverterSignal_counteri_ff1st_nLd_cs_MC_D1_IN1,
      O => counteri_ff1st_nLd_cs_MC_D1_85
    );
  counteri_ff1st_nLd_cs_MC_D2 : X_ZERO
    port map (
      O => counteri_ff1st_nLd_cs_MC_D2_86
    );
  counteri_ff2nd_nLd_cs : X_BUF
    port map (
      I => counteri_ff2nd_nLd_cs_MC_Q,
      O => counteri_ff2nd_nLd_cs_74
    );
  counteri_ff2nd_nLd_cs_MC_REG : X_FF
    generic map(
      INIT => '1'
    )
    port map (
      I => NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_ff2nd_nLd_cs_MC_Q
    );
  counteri_ff2nd_nLd_cs_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_counteri_ff2nd_nLd_cs_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_D_IN1,
      O => counteri_ff2nd_nLd_cs_MC_D_88
    );
  counteri_ff2nd_nLd_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_D1_IN0,
      I1 => NlwInverterSignal_counteri_ff2nd_nLd_cs_MC_D1_IN1,
      O => counteri_ff2nd_nLd_cs_MC_D1_89
    );
  counteri_ff2nd_nLd_cs_MC_D2 : X_ZERO
    port map (
      O => counteri_ff2nd_nLd_cs_MC_D2_90
    );
  counteri_valLd_cs_0_Q : X_BUF
    port map (
      I => counteri_valLd_cs_0_MC_Q,
      O => counteri_valLd_cs(0)
    );
  counteri_valLd_cs_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_0_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_valLd_cs_0_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_valLd_cs_0_MC_Q
    );
  counteri_valLd_cs_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN1,
      O => counteri_valLd_cs_0_MC_D_92
    );
  counteri_valLd_cs_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN1,
      O => counteri_valLd_cs_0_MC_D1_93
    );
  counteri_valLd_cs_0_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_0_MC_D2_94
    );
  counteri_ff1st_updown_cs_1_Q : X_BUF
    port map (
      I => counteri_ff1st_updown_cs_1_MC_Q,
      O => counteri_ff1st_updown_cs(1)
    );
  counteri_ff1st_updown_cs_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_ff1st_updown_cs_1_MC_Q
    );
  counteri_ff1st_updown_cs_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_D_IN1,
      O => counteri_ff1st_updown_cs_1_MC_D_96
    );
  counteri_ff1st_updown_cs_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_D1_IN1,
      O => counteri_ff1st_updown_cs_1_MC_D1_97
    );
  counteri_ff1st_updown_cs_1_MC_D2 : X_ZERO
    port map (
      O => counteri_ff1st_updown_cs_1_MC_D2_98
    );
  counteri_ff2nd_updown_cs_1_Q : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs_1_MC_Q,
      O => counteri_ff2nd_updown_cs(1)
    );
  counteri_ff2nd_updown_cs_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_ff2nd_updown_cs_1_MC_Q
    );
  counteri_ff2nd_updown_cs_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_D_IN1,
      O => counteri_ff2nd_updown_cs_1_MC_D_100
    );
  counteri_ff2nd_updown_cs_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_D1_IN1,
      O => counteri_ff2nd_updown_cs_1_MC_D1_101
    );
  counteri_ff2nd_updown_cs_1_MC_D2 : X_ZERO
    port map (
      O => counteri_ff2nd_updown_cs_1_MC_D2_102
    );
  N_PZ_162 : X_BUF
    port map (
      I => N_PZ_162_MC_Q_103,
      O => N_PZ_162_79
    );
  N_PZ_162_MC_Q : X_BUF
    port map (
      I => N_PZ_162_MC_D_104,
      O => N_PZ_162_MC_Q_103
    );
  N_PZ_162_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_162_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_162_MC_D_IN1,
      O => N_PZ_162_MC_D_104
    );
  N_PZ_162_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_162_MC_D1_IN0,
      I1 => NlwInverterSignal_N_PZ_162_MC_D1_IN1,
      O => N_PZ_162_MC_D1_105
    );
  N_PZ_162_MC_D2 : X_ZERO
    port map (
      O => N_PZ_162_MC_D2_106
    );
  counteri_ff1st_updown_cs_0_Q : X_BUF
    port map (
      I => counteri_ff1st_updown_cs_0_MC_Q,
      O => counteri_ff1st_updown_cs(0)
    );
  counteri_ff1st_updown_cs_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_ff1st_updown_cs_0_MC_Q
    );
  counteri_ff1st_updown_cs_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_D_IN1,
      O => counteri_ff1st_updown_cs_0_MC_D_110
    );
  counteri_ff1st_updown_cs_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_D1_IN1,
      O => counteri_ff1st_updown_cs_0_MC_D1_111
    );
  counteri_ff1st_updown_cs_0_MC_D2 : X_ZERO
    port map (
      O => counteri_ff1st_updown_cs_0_MC_D2_112
    );
  counteri_ff2nd_updown_cs_0_Q : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs_0_MC_Q,
      O => counteri_ff2nd_updown_cs(0)
    );
  counteri_ff2nd_updown_cs_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_ff2nd_updown_cs_0_MC_Q
    );
  counteri_ff2nd_updown_cs_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_D_IN1,
      O => counteri_ff2nd_updown_cs_0_MC_D_114
    );
  counteri_ff2nd_updown_cs_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_D1_IN1,
      O => counteri_ff2nd_updown_cs_0_MC_D1_115
    );
  counteri_ff2nd_updown_cs_0_MC_D2 : X_ZERO
    port map (
      O => counteri_ff2nd_updown_cs_0_MC_D2_116
    );
  N_PZ_136 : X_BUF
    port map (
      I => N_PZ_136_MC_Q_117,
      O => N_PZ_136_80
    );
  N_PZ_136_MC_Q : X_BUF
    port map (
      I => N_PZ_136_MC_D_118,
      O => N_PZ_136_MC_Q_117
    );
  N_PZ_136_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_136_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_136_MC_D_IN1,
      O => N_PZ_136_MC_D_118
    );
  N_PZ_136_MC_D1 : X_ZERO
    port map (
      O => N_PZ_136_MC_D1_119
    );
  N_PZ_136_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_136_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_136_MC_D2_PT_0_IN1,
      O => N_PZ_136_MC_D2_PT_0_121
    );
  N_PZ_136_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_136_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_N_PZ_136_MC_D2_PT_1_IN1,
      O => N_PZ_136_MC_D2_PT_1_122
    );
  N_PZ_136_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_136_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_136_MC_D2_IN1,
      O => N_PZ_136_MC_D2_120
    );
  N_PZ_139 : X_BUF
    port map (
      I => N_PZ_139_MC_Q_123,
      O => N_PZ_139_70
    );
  N_PZ_139_MC_Q : X_BUF
    port map (
      I => N_PZ_139_MC_D_124,
      O => N_PZ_139_MC_Q_123
    );
  N_PZ_139_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_139_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_139_MC_D_IN1,
      O => N_PZ_139_MC_D_124
    );
  N_PZ_139_MC_D1 : X_ZERO
    port map (
      O => N_PZ_139_MC_D1_125
    );
  N_PZ_139_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwInverterSignal_N_PZ_139_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_139_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_N_PZ_139_MC_D2_PT_0_IN2,
      O => N_PZ_139_MC_D2_PT_0_127
    );
  N_PZ_139_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_139_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_N_PZ_139_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_N_PZ_139_MC_D2_PT_1_IN2,
      O => N_PZ_139_MC_D2_PT_1_128
    );
  N_PZ_139_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_139_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_139_MC_D2_IN1,
      O => N_PZ_139_MC_D2_126
    );
  cnt_10_MC_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_tsimrenamed_net_Q,
      O => cnt_10_MC_Q_39
    );
  cnt_10_MC_UIM : X_BUF
    port map (
      I => cnt_10_MC_Q_tsimrenamed_net_Q,
      O => cnt_10_MC_UIM_130
    );
  cnt_10_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_tsimcreated_xor_IN1,
      O => cnt_10_MC_tsimcreated_xor_Q_132
    );
  cnt_10_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_10_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_cnt_10_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => cnt_10_MC_Q_tsimrenamed_net_Q
    );
  cnt_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D_IN1,
      O => cnt_10_MC_D_131
    );
  cnt_10_MC_D1 : X_ZERO
    port map (
      O => cnt_10_MC_D1_133
    );
  cnt_10_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1,
      O => cnt_10_MC_D2_PT_0_135
    );
  cnt_10_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN2,
      O => cnt_10_MC_D2_PT_1_137
    );
  cnt_10_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN3,
      O => cnt_10_MC_D2_PT_2_138
    );
  cnt_10_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN4,
      O => cnt_10_MC_D2_PT_3_143
    );
  cnt_10_MC_D2_PT_4 : X_AND6
    port map (
      I0 => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN5,
      O => cnt_10_MC_D2_PT_4_144
    );
  cnt_10_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_10_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_10_MC_D2_IN4,
      O => cnt_10_MC_D2_134
    );
  counteri_valLd_cs_10_Q : X_BUF
    port map (
      I => counteri_valLd_cs_10_MC_Q,
      O => counteri_valLd_cs(10)
    );
  counteri_valLd_cs_10_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_10_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_valLd_cs_10_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_valLd_cs_10_MC_Q
    );
  counteri_valLd_cs_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN1,
      O => counteri_valLd_cs_10_MC_D_146
    );
  counteri_valLd_cs_10_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN1,
      O => counteri_valLd_cs_10_MC_D1_147
    );
  counteri_valLd_cs_10_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_10_MC_D2_148
    );
  cnt_7_MC_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_tsimrenamed_net_Q,
      O => cnt_7_MC_Q_55
    );
  cnt_7_MC_UIM : X_BUF
    port map (
      I => cnt_7_MC_Q_tsimrenamed_net_Q,
      O => cnt_7_MC_UIM_139
    );
  cnt_7_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN1,
      O => cnt_7_MC_tsimcreated_xor_Q_151
    );
  cnt_7_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_7_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_cnt_7_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => cnt_7_MC_Q_tsimrenamed_net_Q
    );
  cnt_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D_IN1,
      O => cnt_7_MC_D_150
    );
  cnt_7_MC_D1 : X_ZERO
    port map (
      O => cnt_7_MC_D1_152
    );
  cnt_7_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1,
      O => cnt_7_MC_D2_PT_0_154
    );
  cnt_7_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_7_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1,
      O => cnt_7_MC_D2_PT_1_155
    );
  cnt_7_MC_D2_PT_2 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_7_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_7_MC_D2_PT_2_IN2,
      O => cnt_7_MC_D2_PT_2_156
    );
  cnt_7_MC_D2_PT_3 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_7_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_7_MC_D2_PT_3_IN2,
      O => cnt_7_MC_D2_PT_3_158
    );
  cnt_7_MC_D2_PT_4 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN3,
      O => cnt_7_MC_D2_PT_4_159
    );
  cnt_7_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_7_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_7_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_7_MC_D2_IN4,
      O => cnt_7_MC_D2_153
    );
  counteri_valLd_cs_7_Q : X_BUF
    port map (
      I => counteri_valLd_cs_7_MC_Q,
      O => counteri_valLd_cs(7)
    );
  counteri_valLd_cs_7_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_7_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_valLd_cs_7_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_valLd_cs_7_MC_Q
    );
  counteri_valLd_cs_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN1,
      O => counteri_valLd_cs_7_MC_D_161
    );
  counteri_valLd_cs_7_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN1,
      O => counteri_valLd_cs_7_MC_D1_162
    );
  counteri_valLd_cs_7_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_7_MC_D2_163
    );
  counteri_Madd_result_v_add0000_or0005 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_MC_Q_164,
      O => counteri_Madd_result_v_add0000_or0005_140
    );
  counteri_Madd_result_v_add0000_or0005_MC_Q : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_MC_D_165,
      O => counteri_Madd_result_v_add0000_or0005_MC_Q_164
    );
  counteri_Madd_result_v_add0000_or0005_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D_IN1,
      O => counteri_Madd_result_v_add0000_or0005_MC_D_165
    );
  counteri_Madd_result_v_add0000_or0005_MC_D1 : X_AND2
    port map (
      I0 => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D1_IN0,
      I1 => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D1_IN1,
      O => counteri_Madd_result_v_add0000_or0005_MC_D1_166
    );
  counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_IN2,
      O => counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_168
    );
  counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1 : X_AND8
    port map (
      I0 => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN4,
      I5 => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN5,
      I6 => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN6,
      I7 => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN7,
      O => counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_175
    );
  counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2 : X_AND16
    port map (
      I0 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN4,
      I5 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN5,
      I6 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN6,
      I7 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN7,
      I8 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN8,
      I9 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN9,
      I10 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN10,
      I11 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN11,
      I12 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN12,
      I13 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN13,
      I14 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN14,
      I15 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN15,
      O => counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_176
    );
  counteri_Madd_result_v_add0000_or0005_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_IN0,
      I1 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_IN1,
      I2 => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_IN2,
      O => counteri_Madd_result_v_add0000_or0005_MC_D2_167
    );
  cnt_1_MC_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => cnt_1_MC_Q_43
    );
  cnt_1_MC_UIM : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => cnt_1_MC_UIM_169
    );
  cnt_1_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN1,
      O => cnt_1_MC_tsimcreated_xor_Q_179
    );
  cnt_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_cnt_1_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => cnt_1_MC_Q_tsimrenamed_net_Q
    );
  cnt_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D_IN1,
      O => cnt_1_MC_D_178
    );
  cnt_1_MC_D1 : X_ZERO
    port map (
      O => cnt_1_MC_D1_180
    );
  cnt_1_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN1,
      O => cnt_1_MC_D2_PT_0_182
    );
  cnt_1_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1,
      O => cnt_1_MC_D2_PT_1_183
    );
  cnt_1_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN3,
      O => cnt_1_MC_D2_PT_2_185
    );
  cnt_1_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN4,
      O => cnt_1_MC_D2_PT_3_186
    );
  cnt_1_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN4,
      O => cnt_1_MC_D2_PT_4_187
    );
  cnt_1_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_MC_D2_IN4,
      O => cnt_1_MC_D2_181
    );
  counteri_valLd_cs_1_Q : X_BUF
    port map (
      I => counteri_valLd_cs_1_MC_Q,
      O => counteri_valLd_cs(1)
    );
  counteri_valLd_cs_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_1_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_valLd_cs_1_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_valLd_cs_1_MC_Q
    );
  counteri_valLd_cs_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN1,
      O => counteri_valLd_cs_1_MC_D_189
    );
  counteri_valLd_cs_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN1,
      O => counteri_valLd_cs_1_MC_D1_190
    );
  counteri_valLd_cs_1_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_1_MC_D2_191
    );
  cnt_2_MC_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => cnt_2_MC_Q_45
    );
  cnt_2_MC_UIM : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => cnt_2_MC_UIM_170
    );
  cnt_2_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN1,
      O => cnt_2_MC_tsimcreated_xor_Q_194
    );
  cnt_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_2_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_cnt_2_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => cnt_2_MC_Q_tsimrenamed_net_Q
    );
  cnt_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D_IN1,
      O => cnt_2_MC_D_193
    );
  cnt_2_MC_D1 : X_ZERO
    port map (
      O => cnt_2_MC_D1_195
    );
  cnt_2_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_2_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1,
      O => cnt_2_MC_D2_PT_0_197
    );
  cnt_2_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2,
      O => cnt_2_MC_D2_PT_1_198
    );
  cnt_2_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN3,
      O => cnt_2_MC_D2_PT_2_200
    );
  cnt_2_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4,
      O => cnt_2_MC_D2_PT_3_201
    );
  cnt_2_MC_D2_PT_4 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN5,
      O => cnt_2_MC_D2_PT_4_202
    );
  cnt_2_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_IN4,
      O => cnt_2_MC_D2_196
    );
  counteri_valLd_cs_2_Q : X_BUF
    port map (
      I => counteri_valLd_cs_2_MC_Q,
      O => counteri_valLd_cs(2)
    );
  counteri_valLd_cs_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_2_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_valLd_cs_2_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_valLd_cs_2_MC_Q
    );
  counteri_valLd_cs_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN1,
      O => counteri_valLd_cs_2_MC_D_204
    );
  counteri_valLd_cs_2_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN1,
      O => counteri_valLd_cs_2_MC_D1_205
    );
  counteri_valLd_cs_2_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_2_MC_D2_206
    );
  cnt_3_MC_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_tsimrenamed_net_Q,
      O => cnt_3_MC_Q_47
    );
  cnt_3_MC_UIM : X_BUF
    port map (
      I => cnt_3_MC_Q_tsimrenamed_net_Q,
      O => cnt_3_MC_UIM_171
    );
  cnt_3_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN1,
      O => cnt_3_MC_tsimcreated_xor_Q_209
    );
  cnt_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_3_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_cnt_3_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => cnt_3_MC_Q_tsimrenamed_net_Q
    );
  cnt_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D_IN1,
      O => cnt_3_MC_D_208
    );
  cnt_3_MC_D1 : X_ZERO
    port map (
      O => cnt_3_MC_D1_210
    );
  cnt_3_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_3_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1,
      O => cnt_3_MC_D2_PT_0_212
    );
  cnt_3_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN3,
      O => cnt_3_MC_D2_PT_1_213
    );
  cnt_3_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN3,
      O => cnt_3_MC_D2_PT_2_215
    );
  cnt_3_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_3_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_3_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN4,
      O => cnt_3_MC_D2_PT_3_216
    );
  cnt_3_MC_D2_PT_4 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN6,
      O => cnt_3_MC_D2_PT_4_217
    );
  cnt_3_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_3_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_IN4,
      O => cnt_3_MC_D2_211
    );
  counteri_valLd_cs_3_Q : X_BUF
    port map (
      I => counteri_valLd_cs_3_MC_Q,
      O => counteri_valLd_cs(3)
    );
  counteri_valLd_cs_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_3_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_valLd_cs_3_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_valLd_cs_3_MC_Q
    );
  counteri_valLd_cs_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN1,
      O => counteri_valLd_cs_3_MC_D_219
    );
  counteri_valLd_cs_3_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN1,
      O => counteri_valLd_cs_3_MC_D1_220
    );
  counteri_valLd_cs_3_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_3_MC_D2_221
    );
  cnt_4_MC_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_tsimrenamed_net_Q,
      O => cnt_4_MC_Q_49
    );
  cnt_4_MC_UIM : X_BUF
    port map (
      I => cnt_4_MC_Q_tsimrenamed_net_Q,
      O => cnt_4_MC_UIM_172
    );
  cnt_4_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN1,
      O => cnt_4_MC_tsimcreated_xor_Q_224
    );
  cnt_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_4_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_cnt_4_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => cnt_4_MC_Q_tsimrenamed_net_Q
    );
  cnt_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D_IN1,
      O => cnt_4_MC_D_223
    );
  cnt_4_MC_D1 : X_ZERO
    port map (
      O => cnt_4_MC_D1_225
    );
  cnt_4_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1,
      O => cnt_4_MC_D2_PT_0_227
    );
  cnt_4_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN3,
      O => cnt_4_MC_D2_PT_1_229
    );
  cnt_4_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN4,
      O => cnt_4_MC_D2_PT_2_230
    );
  cnt_4_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4,
      O => cnt_4_MC_D2_PT_3_231
    );
  cnt_4_MC_D2_PT_4 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN7,
      O => cnt_4_MC_D2_PT_4_232
    );
  cnt_4_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_4_MC_D2_IN4,
      O => cnt_4_MC_D2_226
    );
  counteri_valLd_cs_4_Q : X_BUF
    port map (
      I => counteri_valLd_cs_4_MC_Q,
      O => counteri_valLd_cs(4)
    );
  counteri_valLd_cs_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_4_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_valLd_cs_4_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_valLd_cs_4_MC_Q
    );
  counteri_valLd_cs_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN1,
      O => counteri_valLd_cs_4_MC_D_234
    );
  counteri_valLd_cs_4_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN1,
      O => counteri_valLd_cs_4_MC_D1_235
    );
  counteri_valLd_cs_4_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_4_MC_D2_236
    );
  cnt_5_MC_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_tsimrenamed_net_Q,
      O => cnt_5_MC_Q_51
    );
  cnt_5_MC_UIM : X_BUF
    port map (
      I => cnt_5_MC_Q_tsimrenamed_net_Q,
      O => cnt_5_MC_UIM_173
    );
  cnt_5_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN1,
      O => cnt_5_MC_tsimcreated_xor_Q_239
    );
  cnt_5_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_5_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_cnt_5_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => cnt_5_MC_Q_tsimrenamed_net_Q
    );
  cnt_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D_IN1,
      O => cnt_5_MC_D_238
    );
  cnt_5_MC_D1 : X_ZERO
    port map (
      O => cnt_5_MC_D1_240
    );
  cnt_5_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_5_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1,
      O => cnt_5_MC_D2_PT_0_242
    );
  cnt_5_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN3,
      O => cnt_5_MC_D2_PT_1_244
    );
  cnt_5_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN4,
      O => cnt_5_MC_D2_PT_2_245
    );
  cnt_5_MC_D2_PT_3 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN5,
      O => cnt_5_MC_D2_PT_3_246
    );
  cnt_5_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN15,
      O => cnt_5_MC_D2_PT_4_247
    );
  cnt_5_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_IN4,
      O => cnt_5_MC_D2_241
    );
  counteri_valLd_cs_5_Q : X_BUF
    port map (
      I => counteri_valLd_cs_5_MC_Q,
      O => counteri_valLd_cs(5)
    );
  counteri_valLd_cs_5_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_5_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_valLd_cs_5_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_valLd_cs_5_MC_Q
    );
  counteri_valLd_cs_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN1,
      O => counteri_valLd_cs_5_MC_D_249
    );
  counteri_valLd_cs_5_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN1,
      O => counteri_valLd_cs_5_MC_D1_250
    );
  counteri_valLd_cs_5_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_5_MC_D2_251
    );
  cnt_6_MC_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_tsimrenamed_net_Q,
      O => cnt_6_MC_Q_53
    );
  cnt_6_MC_UIM : X_BUF
    port map (
      I => cnt_6_MC_Q_tsimrenamed_net_Q,
      O => cnt_6_MC_UIM_174
    );
  cnt_6_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN1,
      O => cnt_6_MC_tsimcreated_xor_Q_254
    );
  cnt_6_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_6_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_cnt_6_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => cnt_6_MC_Q_tsimrenamed_net_Q
    );
  cnt_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D_IN1,
      O => cnt_6_MC_D_253
    );
  cnt_6_MC_D1 : X_ZERO
    port map (
      O => cnt_6_MC_D1_255
    );
  cnt_6_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1,
      O => cnt_6_MC_D2_PT_0_257
    );
  cnt_6_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN2,
      O => cnt_6_MC_D2_PT_1_259
    );
  cnt_6_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3,
      O => cnt_6_MC_D2_PT_2_260
    );
  cnt_6_MC_D2_PT_3 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN6,
      O => cnt_6_MC_D2_PT_3_261
    );
  cnt_6_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN15,
      O => cnt_6_MC_D2_PT_4_262
    );
  cnt_6_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_IN4,
      O => cnt_6_MC_D2_256
    );
  counteri_valLd_cs_6_Q : X_BUF
    port map (
      I => counteri_valLd_cs_6_MC_Q,
      O => counteri_valLd_cs(6)
    );
  counteri_valLd_cs_6_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_6_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_valLd_cs_6_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_valLd_cs_6_MC_Q
    );
  counteri_valLd_cs_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN1,
      O => counteri_valLd_cs_6_MC_D_264
    );
  counteri_valLd_cs_6_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN1,
      O => counteri_valLd_cs_6_MC_D1_265
    );
  counteri_valLd_cs_6_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_6_MC_D2_266
    );
  cnt_8_MC_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_tsimrenamed_net_Q,
      O => cnt_8_MC_Q_57
    );
  cnt_8_MC_UIM : X_BUF
    port map (
      I => cnt_8_MC_Q_tsimrenamed_net_Q,
      O => cnt_8_MC_UIM_141
    );
  cnt_8_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN1,
      O => cnt_8_MC_tsimcreated_xor_Q_269
    );
  cnt_8_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_8_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_cnt_8_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => cnt_8_MC_Q_tsimrenamed_net_Q
    );
  cnt_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D_IN1,
      O => cnt_8_MC_D_268
    );
  cnt_8_MC_D1 : X_ZERO
    port map (
      O => cnt_8_MC_D1_270
    );
  cnt_8_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1,
      O => cnt_8_MC_D2_PT_0_272
    );
  cnt_8_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2,
      O => cnt_8_MC_D2_PT_1_273
    );
  cnt_8_MC_D2_PT_2 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_8_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_8_MC_D2_PT_2_IN2,
      O => cnt_8_MC_D2_PT_2_275
    );
  cnt_8_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwInverterSignal_cnt_8_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_8_MC_D2_PT_3_IN3,
      O => cnt_8_MC_D2_PT_3_276
    );
  cnt_8_MC_D2_PT_4 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN3,
      O => cnt_8_MC_D2_PT_4_277
    );
  cnt_8_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_8_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_8_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_8_MC_D2_IN4,
      O => cnt_8_MC_D2_271
    );
  counteri_valLd_cs_8_Q : X_BUF
    port map (
      I => counteri_valLd_cs_8_MC_Q,
      O => counteri_valLd_cs(8)
    );
  counteri_valLd_cs_8_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_8_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_valLd_cs_8_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_valLd_cs_8_MC_Q
    );
  counteri_valLd_cs_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN1,
      O => counteri_valLd_cs_8_MC_D_279
    );
  counteri_valLd_cs_8_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN1,
      O => counteri_valLd_cs_8_MC_D1_280
    );
  counteri_valLd_cs_8_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_8_MC_D2_281
    );
  cnt_9_MC_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_tsimrenamed_net_Q,
      O => cnt_9_MC_Q_59
    );
  cnt_9_MC_UIM : X_BUF
    port map (
      I => cnt_9_MC_Q_tsimrenamed_net_Q,
      O => cnt_9_MC_UIM_142
    );
  cnt_9_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN1,
      O => cnt_9_MC_tsimcreated_xor_Q_284
    );
  cnt_9_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_9_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_cnt_9_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => cnt_9_MC_Q_tsimrenamed_net_Q
    );
  cnt_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D_IN1,
      O => cnt_9_MC_D_283
    );
  cnt_9_MC_D1 : X_ZERO
    port map (
      O => cnt_9_MC_D1_285
    );
  cnt_9_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1,
      O => cnt_9_MC_D2_PT_0_287
    );
  cnt_9_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN2,
      O => cnt_9_MC_D2_PT_1_289
    );
  cnt_9_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN3,
      O => cnt_9_MC_D2_PT_2_290
    );
  cnt_9_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3,
      O => cnt_9_MC_D2_PT_3_291
    );
  cnt_9_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN4,
      O => cnt_9_MC_D2_PT_4_292
    );
  cnt_9_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_9_MC_D2_IN4,
      O => cnt_9_MC_D2_286
    );
  counteri_valLd_cs_9_Q : X_BUF
    port map (
      I => counteri_valLd_cs_9_MC_Q,
      O => counteri_valLd_cs(9)
    );
  counteri_valLd_cs_9_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_9_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_valLd_cs_9_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_valLd_cs_9_MC_Q
    );
  counteri_valLd_cs_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN1,
      O => counteri_valLd_cs_9_MC_D_294
    );
  counteri_valLd_cs_9_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN1,
      O => counteri_valLd_cs_9_MC_D1_295
    );
  counteri_valLd_cs_9_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_9_MC_D2_296
    );
  cnt_11_MC_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_tsimrenamed_net_Q,
      O => cnt_11_MC_Q_41
    );
  cnt_11_MC_UIM : X_BUF
    port map (
      I => cnt_11_MC_Q_tsimrenamed_net_Q,
      O => cnt_11_MC_UIM_298
    );
  cnt_11_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN1,
      O => cnt_11_MC_tsimcreated_xor_Q_300
    );
  cnt_11_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_11_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_cnt_11_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => cnt_11_MC_Q_tsimrenamed_net_Q
    );
  cnt_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D_IN1,
      O => cnt_11_MC_D_299
    );
  cnt_11_MC_D1 : X_ZERO
    port map (
      O => cnt_11_MC_D1_301
    );
  cnt_11_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1,
      O => cnt_11_MC_D2_PT_0_303
    );
  cnt_11_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2,
      O => cnt_11_MC_D2_PT_1_305
    );
  cnt_11_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3,
      O => cnt_11_MC_D2_PT_2_306
    );
  cnt_11_MC_D2_PT_3 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN5,
      O => cnt_11_MC_D2_PT_3_307
    );
  cnt_11_MC_D2_PT_4 : X_AND7
    port map (
      I0 => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN6,
      O => cnt_11_MC_D2_PT_4_308
    );
  cnt_11_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_IN4,
      O => cnt_11_MC_D2_302
    );
  counteri_valLd_cs_11_Q : X_BUF
    port map (
      I => counteri_valLd_cs_11_MC_Q,
      O => counteri_valLd_cs(11)
    );
  counteri_valLd_cs_11_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_counteri_valLd_cs_11_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_valLd_cs_11_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_valLd_cs_11_MC_Q
    );
  counteri_valLd_cs_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN1,
      O => counteri_valLd_cs_11_MC_D_310
    );
  counteri_valLd_cs_11_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN0,
      I1 => NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN1,
      O => counteri_valLd_cs_11_MC_D1_311
    );
  counteri_valLd_cs_11_MC_D2 : X_ZERO
    port map (
      O => counteri_valLd_cs_11_MC_D2_312
    );
  err_MC_Q : X_BUF
    port map (
      I => err_MC_Q_tsimrenamed_net_Q,
      O => err_MC_Q_61
    );
  err_MC_UIM : X_BUF
    port map (
      I => err_MC_Q_tsimrenamed_net_Q,
      O => err_MC_UIM_314
    );
  err_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_err_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_err_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => err_MC_Q_tsimrenamed_net_Q
    );
  err_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_err_MC_D_IN0,
      I1 => NlwBufferSignal_err_MC_D_IN1,
      O => err_MC_D_315
    );
  err_MC_D1 : X_ZERO
    port map (
      O => err_MC_D1_316
    );
  err_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_0_IN2,
      O => err_MC_D2_PT_0_319
    );
  err_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_1_IN2,
      O => err_MC_D2_PT_1_321
    );
  err_MC_D2_PT_2 : X_AND8
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_err_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_err_MC_D2_PT_2_IN4,
      I5 => NlwInverterSignal_err_MC_D2_PT_2_IN5,
      I6 => NlwInverterSignal_err_MC_D2_PT_2_IN6,
      I7 => NlwBufferSignal_err_MC_D2_PT_2_IN7,
      O => err_MC_D2_PT_2_322
    );
  err_MC_D2_PT_3 : X_AND8
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_err_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_err_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_err_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_err_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_err_MC_D2_PT_3_IN7,
      O => err_MC_D2_PT_3_323
    );
  err_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwInverterSignal_err_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_err_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_err_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_err_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_err_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_err_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_err_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_err_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_err_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_err_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_err_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_err_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_err_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_err_MC_D2_PT_4_IN15,
      O => err_MC_D2_PT_4_324
    );
  err_MC_D2_PT_5 : X_AND16
    port map (
      I0 => NlwInverterSignal_err_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_err_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_err_MC_D2_PT_5_IN4,
      I5 => NlwBufferSignal_err_MC_D2_PT_5_IN5,
      I6 => NlwBufferSignal_err_MC_D2_PT_5_IN6,
      I7 => NlwBufferSignal_err_MC_D2_PT_5_IN7,
      I8 => NlwInverterSignal_err_MC_D2_PT_5_IN8,
      I9 => NlwBufferSignal_err_MC_D2_PT_5_IN9,
      I10 => NlwBufferSignal_err_MC_D2_PT_5_IN10,
      I11 => NlwBufferSignal_err_MC_D2_PT_5_IN11,
      I12 => NlwBufferSignal_err_MC_D2_PT_5_IN12,
      I13 => NlwBufferSignal_err_MC_D2_PT_5_IN13,
      I14 => NlwBufferSignal_err_MC_D2_PT_5_IN14,
      I15 => NlwBufferSignal_err_MC_D2_PT_5_IN15,
      O => err_MC_D2_PT_5_325
    );
  err_MC_D2 : X_OR6
    port map (
      I0 => NlwBufferSignal_err_MC_D2_IN0,
      I1 => NlwBufferSignal_err_MC_D2_IN1,
      I2 => NlwBufferSignal_err_MC_D2_IN2,
      I3 => NlwBufferSignal_err_MC_D2_IN3,
      I4 => NlwBufferSignal_err_MC_D2_IN4,
      I5 => NlwBufferSignal_err_MC_D2_IN5,
      O => err_MC_D2_317
    );
  counteri_ff1st_nClr_cs : X_BUF
    port map (
      I => counteri_ff1st_nClr_cs_MC_Q,
      O => counteri_ff1st_nClr_cs_318
    );
  counteri_ff1st_nClr_cs_MC_REG : X_FF
    generic map(
      INIT => '1'
    )
    port map (
      I => NlwBufferSignal_counteri_ff1st_nClr_cs_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_ff1st_nClr_cs_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_ff1st_nClr_cs_MC_Q
    );
  counteri_ff1st_nClr_cs_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_counteri_ff1st_nClr_cs_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_ff1st_nClr_cs_MC_D_IN1,
      O => counteri_ff1st_nClr_cs_MC_D_327
    );
  counteri_ff1st_nClr_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_ff1st_nClr_cs_MC_D1_IN0,
      I1 => NlwInverterSignal_counteri_ff1st_nClr_cs_MC_D1_IN1,
      O => counteri_ff1st_nClr_cs_MC_D1_328
    );
  counteri_ff1st_nClr_cs_MC_D2 : X_ZERO
    port map (
      O => counteri_ff1st_nClr_cs_MC_D2_329
    );
  counteri_ff2nd_nClr_cs : X_BUF
    port map (
      I => counteri_ff2nd_nClr_cs_MC_Q,
      O => counteri_ff2nd_nClr_cs_320
    );
  counteri_ff2nd_nClr_cs_MC_REG : X_FF
    generic map(
      INIT => '1'
    )
    port map (
      I => NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_REG_IN,
      CE => Vcc_67,
      CLK => NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_REG_CLK,
      SET => Gnd_66,
      RST => Gnd_66,
      O => counteri_ff2nd_nClr_cs_MC_Q
    );
  counteri_ff2nd_nClr_cs_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_counteri_ff2nd_nClr_cs_MC_D_IN0,
      I1 => NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_D_IN1,
      O => counteri_ff2nd_nClr_cs_MC_D_331
    );
  counteri_ff2nd_nClr_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_D1_IN0,
      I1 => NlwInverterSignal_counteri_ff2nd_nClr_cs_MC_D1_IN1,
      O => counteri_ff2nd_nClr_cs_MC_D1_332
    );
  counteri_ff2nd_nClr_cs_MC_D2 : X_ZERO
    port map (
      O => counteri_ff2nd_nClr_cs_MC_D2_333
    );
  NlwBufferBlock_cnt_0_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D_64,
      O => NlwBufferSignal_cnt_0_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_0_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_0_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_0_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_0_MC_REG_IN : X_BUF
    port map (
      I => cnt_0_MC_tsimcreated_xor_Q_65,
      O => NlwBufferSignal_cnt_0_MC_REG_IN
    );
  NlwBufferBlock_cnt_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_0_MC_REG_CLK
    );
  NlwBufferBlock_cnt_0_MC_D_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D1_68,
      O => NlwBufferSignal_cnt_0_MC_D_IN0
    );
  NlwBufferBlock_cnt_0_MC_D_IN1 : X_BUF
    port map (
      I => cnt_0_MC_D2_69,
      O => NlwBufferSignal_cnt_0_MC_D_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_73,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => counteri_ff2nd_nLd_cs_74,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(0),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => counteri_ff1st_updown_cs(1),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs(1),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_162_79,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_73,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_ff2nd_nLd_cs_74,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_valLd_cs(0),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_0_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_0_71,
      O => NlwBufferSignal_cnt_0_MC_D2_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_1_72,
      O => NlwBufferSignal_cnt_0_MC_D2_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_2_76,
      O => NlwBufferSignal_cnt_0_MC_D2_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_3_81,
      O => NlwBufferSignal_cnt_0_MC_D2_IN3
    );
  NlwBufferBlock_cnt_0_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_4_82,
      O => NlwBufferSignal_cnt_0_MC_D2_IN4
    );
  NlwBufferBlock_counteri_ff1st_nLd_cs_MC_REG_IN : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_MC_D_84,
      O => NlwBufferSignal_counteri_ff1st_nLd_cs_MC_REG_IN
    );
  NlwBufferBlock_counteri_ff1st_nLd_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_ff1st_nLd_cs_MC_REG_CLK
    );
  NlwBufferBlock_counteri_ff1st_nLd_cs_MC_D_IN0 : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_MC_D1_85,
      O => NlwBufferSignal_counteri_ff1st_nLd_cs_MC_D_IN0
    );
  NlwBufferBlock_counteri_ff1st_nLd_cs_MC_D_IN1 : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_MC_D2_86,
      O => NlwBufferSignal_counteri_ff1st_nLd_cs_MC_D_IN1
    );
  NlwBufferBlock_counteri_ff1st_nLd_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_ff1st_nLd_cs_MC_D1_IN0
    );
  NlwBufferBlock_counteri_ff1st_nLd_cs_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_5,
      O => NlwBufferSignal_counteri_ff1st_nLd_cs_MC_D1_IN1
    );
  NlwBufferBlock_counteri_ff2nd_nLd_cs_MC_REG_IN : X_BUF
    port map (
      I => counteri_ff2nd_nLd_cs_MC_D_88,
      O => NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_REG_IN
    );
  NlwBufferBlock_counteri_ff2nd_nLd_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_REG_CLK
    );
  NlwBufferBlock_counteri_ff2nd_nLd_cs_MC_D_IN0 : X_BUF
    port map (
      I => counteri_ff2nd_nLd_cs_MC_D1_89,
      O => NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_D_IN0
    );
  NlwBufferBlock_counteri_ff2nd_nLd_cs_MC_D_IN1 : X_BUF
    port map (
      I => counteri_ff2nd_nLd_cs_MC_D2_90,
      O => NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_D_IN1
    );
  NlwBufferBlock_counteri_ff2nd_nLd_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_D1_IN0
    );
  NlwBufferBlock_counteri_ff2nd_nLd_cs_MC_D1_IN1 : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_73,
      O => NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_D1_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_0_MC_D_92,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_0_MC_D1_93,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_0_MC_D2_94,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_0_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_0_II_UIM_7,
      O => NlwBufferSignal_counteri_valLd_cs_0_MC_D1_IN1
    );
  NlwBufferBlock_counteri_ff1st_updown_cs_1_MC_REG_IN : X_BUF
    port map (
      I => counteri_ff1st_updown_cs_1_MC_D_96,
      O => NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_REG_IN
    );
  NlwBufferBlock_counteri_ff1st_updown_cs_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_REG_CLK
    );
  NlwBufferBlock_counteri_ff1st_updown_cs_1_MC_D_IN0 : X_BUF
    port map (
      I => counteri_ff1st_updown_cs_1_MC_D1_97,
      O => NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_D_IN0
    );
  NlwBufferBlock_counteri_ff1st_updown_cs_1_MC_D_IN1 : X_BUF
    port map (
      I => counteri_ff1st_updown_cs_1_MC_D2_98,
      O => NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_D_IN1
    );
  NlwBufferBlock_counteri_ff1st_updown_cs_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_D1_IN0
    );
  NlwBufferBlock_counteri_ff1st_updown_cs_1_MC_D1_IN1 : X_BUF
    port map (
      I => up_II_UIM_9,
      O => NlwBufferSignal_counteri_ff1st_updown_cs_1_MC_D1_IN1
    );
  NlwBufferBlock_counteri_ff2nd_updown_cs_1_MC_REG_IN : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs_1_MC_D_100,
      O => NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_REG_IN
    );
  NlwBufferBlock_counteri_ff2nd_updown_cs_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_REG_CLK
    );
  NlwBufferBlock_counteri_ff2nd_updown_cs_1_MC_D_IN0 : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs_1_MC_D1_101,
      O => NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_D_IN0
    );
  NlwBufferBlock_counteri_ff2nd_updown_cs_1_MC_D_IN1 : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs_1_MC_D2_102,
      O => NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_D_IN1
    );
  NlwBufferBlock_counteri_ff2nd_updown_cs_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_D1_IN0
    );
  NlwBufferBlock_counteri_ff2nd_updown_cs_1_MC_D1_IN1 : X_BUF
    port map (
      I => counteri_ff1st_updown_cs(1),
      O => NlwBufferSignal_counteri_ff2nd_updown_cs_1_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_162_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_162_MC_D1_105,
      O => NlwBufferSignal_N_PZ_162_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_162_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_162_MC_D2_106,
      O => NlwBufferSignal_N_PZ_162_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_162_MC_D1_IN0 : X_BUF
    port map (
      I => counteri_ff1st_updown_cs(0),
      O => NlwBufferSignal_N_PZ_162_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_162_MC_D1_IN1 : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs(0),
      O => NlwBufferSignal_N_PZ_162_MC_D1_IN1
    );
  NlwBufferBlock_counteri_ff1st_updown_cs_0_MC_REG_IN : X_BUF
    port map (
      I => counteri_ff1st_updown_cs_0_MC_D_110,
      O => NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_REG_IN
    );
  NlwBufferBlock_counteri_ff1st_updown_cs_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_REG_CLK
    );
  NlwBufferBlock_counteri_ff1st_updown_cs_0_MC_D_IN0 : X_BUF
    port map (
      I => counteri_ff1st_updown_cs_0_MC_D1_111,
      O => NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_D_IN0
    );
  NlwBufferBlock_counteri_ff1st_updown_cs_0_MC_D_IN1 : X_BUF
    port map (
      I => counteri_ff1st_updown_cs_0_MC_D2_112,
      O => NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_D_IN1
    );
  NlwBufferBlock_counteri_ff1st_updown_cs_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_D1_IN0
    );
  NlwBufferBlock_counteri_ff1st_updown_cs_0_MC_D1_IN1 : X_BUF
    port map (
      I => down_II_UIM_11,
      O => NlwBufferSignal_counteri_ff1st_updown_cs_0_MC_D1_IN1
    );
  NlwBufferBlock_counteri_ff2nd_updown_cs_0_MC_REG_IN : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs_0_MC_D_114,
      O => NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_REG_IN
    );
  NlwBufferBlock_counteri_ff2nd_updown_cs_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_REG_CLK
    );
  NlwBufferBlock_counteri_ff2nd_updown_cs_0_MC_D_IN0 : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs_0_MC_D1_115,
      O => NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_D_IN0
    );
  NlwBufferBlock_counteri_ff2nd_updown_cs_0_MC_D_IN1 : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs_0_MC_D2_116,
      O => NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_D_IN1
    );
  NlwBufferBlock_counteri_ff2nd_updown_cs_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_D1_IN0
    );
  NlwBufferBlock_counteri_ff2nd_updown_cs_0_MC_D1_IN1 : X_BUF
    port map (
      I => counteri_ff1st_updown_cs(0),
      O => NlwBufferSignal_counteri_ff2nd_updown_cs_0_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_136_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_136_MC_D1_119,
      O => NlwBufferSignal_N_PZ_136_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_136_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_136_MC_D2_120,
      O => NlwBufferSignal_N_PZ_136_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_136_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_136_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_136_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_73,
      O => NlwBufferSignal_N_PZ_136_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_136_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_136_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_136_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => counteri_ff2nd_nLd_cs_74,
      O => NlwBufferSignal_N_PZ_136_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_136_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_136_MC_D2_PT_0_121,
      O => NlwBufferSignal_N_PZ_136_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_136_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_136_MC_D2_PT_1_122,
      O => NlwBufferSignal_N_PZ_136_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_139_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_139_MC_D1_125,
      O => NlwBufferSignal_N_PZ_139_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_139_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_139_MC_D2_126,
      O => NlwBufferSignal_N_PZ_139_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_139_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => counteri_ff1st_updown_cs(1),
      O => NlwBufferSignal_N_PZ_139_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_139_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_162_79,
      O => NlwBufferSignal_N_PZ_139_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_139_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_N_PZ_139_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_N_PZ_139_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs(1),
      O => NlwBufferSignal_N_PZ_139_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_139_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_162_79,
      O => NlwBufferSignal_N_PZ_139_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_139_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_N_PZ_139_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_139_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_139_MC_D2_PT_0_127,
      O => NlwBufferSignal_N_PZ_139_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_139_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_139_MC_D2_PT_1_128,
      O => NlwBufferSignal_N_PZ_139_MC_D2_IN1
    );
  NlwBufferBlock_cnt_10_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D_131,
      O => NlwBufferSignal_cnt_10_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_10_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_10_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_10_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_10_MC_REG_IN : X_BUF
    port map (
      I => cnt_10_MC_tsimcreated_xor_Q_132,
      O => NlwBufferSignal_cnt_10_MC_REG_IN
    );
  NlwBufferBlock_cnt_10_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_10_MC_REG_CLK
    );
  NlwBufferBlock_cnt_10_MC_D_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D1_133,
      O => NlwBufferSignal_cnt_10_MC_D_IN0
    );
  NlwBufferBlock_cnt_10_MC_D_IN1 : X_BUF
    port map (
      I => cnt_10_MC_D2_134,
      O => NlwBufferSignal_cnt_10_MC_D_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_10_MC_UIM_130,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_10_MC_UIM_130,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_10_MC_UIM_130,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_7_MC_UIM_139,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_140,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_8_MC_UIM_141,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_9_MC_UIM_142,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_7_MC_UIM_139,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_140,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_8_MC_UIM_141,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_9_MC_UIM_142,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_10_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_0_135,
      O => NlwBufferSignal_cnt_10_MC_D2_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_1_137,
      O => NlwBufferSignal_cnt_10_MC_D2_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_2_138,
      O => NlwBufferSignal_cnt_10_MC_D2_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_3_143,
      O => NlwBufferSignal_cnt_10_MC_D2_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_4_144,
      O => NlwBufferSignal_cnt_10_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_10_MC_D_146,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_10_MC_D1_147,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_10_MC_D2_148,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_10_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_10_II_UIM_13,
      O => NlwBufferSignal_counteri_valLd_cs_10_MC_D1_IN1
    );
  NlwBufferBlock_cnt_7_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D_150,
      O => NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_7_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_7_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_7_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_7_MC_REG_IN : X_BUF
    port map (
      I => cnt_7_MC_tsimcreated_xor_Q_151,
      O => NlwBufferSignal_cnt_7_MC_REG_IN
    );
  NlwBufferBlock_cnt_7_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_7_MC_REG_CLK
    );
  NlwBufferBlock_cnt_7_MC_D_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D1_152,
      O => NlwBufferSignal_cnt_7_MC_D_IN0
    );
  NlwBufferBlock_cnt_7_MC_D_IN1 : X_BUF
    port map (
      I => cnt_7_MC_D2_153,
      O => NlwBufferSignal_cnt_7_MC_D_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_140,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_7_MC_UIM_139,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_140,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_7_MC_UIM_139,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(7),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_7_MC_UIM_139,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(7),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_0_154,
      O => NlwBufferSignal_cnt_7_MC_D2_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_1_155,
      O => NlwBufferSignal_cnt_7_MC_D2_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_2_156,
      O => NlwBufferSignal_cnt_7_MC_D2_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_3_158,
      O => NlwBufferSignal_cnt_7_MC_D2_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_4_159,
      O => NlwBufferSignal_cnt_7_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_7_MC_D_161,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_7_MC_D1_162,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_7_MC_D2_163,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_7_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_7_II_UIM_15,
      O => NlwBufferSignal_counteri_valLd_cs_7_MC_D1_IN1
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D_IN0 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_MC_D1_166,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D_IN0
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D_IN1 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_MC_D2_167,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D_IN1
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D1_IN0 : X_BUF
    port map (
      I => N_PZ_162_79,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D1_IN0
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D1_IN1 : X_BUF
    port map (
      I => N_PZ_162_79,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D1_IN1
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => counteri_ff1st_updown_cs(1),
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs(1),
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_162_79,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_162_79,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_MC_UIM_169,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_170,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_4_MC_UIM_172,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_5_MC_UIM_173,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN7 : X_BUF
    port map (
      I => cnt_6_MC_UIM_174,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN7
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_ff1st_updown_cs(1),
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs(1),
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_162_79,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_1_MC_UIM_169,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_170,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN7 : X_BUF
    port map (
      I => cnt_4_MC_UIM_172,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN7
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN8 : X_BUF
    port map (
      I => cnt_5_MC_UIM_173,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN8
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN9 : X_BUF
    port map (
      I => cnt_6_MC_UIM_174,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN9
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN10 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN10
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN11 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN11
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN12 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN12
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN13 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN13
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN14 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN14
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN15 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN15
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_IN0 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_168,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_IN0
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_IN1 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_175,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_IN1
    );
  NlwBufferBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_IN2 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_176,
      O => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D_178,
      O => NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_MC_tsimcreated_xor_Q_179,
      O => NlwBufferSignal_cnt_1_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D1_180,
      O => NlwBufferSignal_cnt_1_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_181,
      O => NlwBufferSignal_cnt_1_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_MC_UIM_169,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_ff2nd_nLd_cs_74,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_MC_UIM_169,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_ff2nd_nLd_cs_74,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_MC_UIM_169,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => counteri_ff1st_updown_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => N_PZ_162_79,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_0_182,
      O => NlwBufferSignal_cnt_1_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_1_183,
      O => NlwBufferSignal_cnt_1_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_2_185,
      O => NlwBufferSignal_cnt_1_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_3_186,
      O => NlwBufferSignal_cnt_1_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_4_187,
      O => NlwBufferSignal_cnt_1_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_1_MC_D_189,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_1_MC_D1_190,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_1_MC_D2_191,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_1_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_1_II_UIM_17,
      O => NlwBufferSignal_counteri_valLd_cs_1_MC_D1_IN1
    );
  NlwBufferBlock_cnt_2_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D_193,
      O => NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_2_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_2_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_2_MC_REG_IN : X_BUF
    port map (
      I => cnt_2_MC_tsimcreated_xor_Q_194,
      O => NlwBufferSignal_cnt_2_MC_REG_IN
    );
  NlwBufferBlock_cnt_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_2_MC_REG_CLK
    );
  NlwBufferBlock_cnt_2_MC_D_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D1_195,
      O => NlwBufferSignal_cnt_2_MC_D_IN0
    );
  NlwBufferBlock_cnt_2_MC_D_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_196,
      O => NlwBufferSignal_cnt_2_MC_D_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_2_MC_UIM_170,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_MC_UIM_169,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_ff2nd_nLd_cs_74,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_2_MC_UIM_170,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_ff2nd_nLd_cs_74,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_170,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => counteri_ff1st_updown_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => N_PZ_162_79,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_169,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_2_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_0_197,
      O => NlwBufferSignal_cnt_2_MC_D2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_1_198,
      O => NlwBufferSignal_cnt_2_MC_D2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_2_200,
      O => NlwBufferSignal_cnt_2_MC_D2_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_3_201,
      O => NlwBufferSignal_cnt_2_MC_D2_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_4_202,
      O => NlwBufferSignal_cnt_2_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_2_MC_D_204,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_2_MC_D1_205,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_2_MC_D2_206,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_2_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_2_II_UIM_19,
      O => NlwBufferSignal_counteri_valLd_cs_2_MC_D1_IN1
    );
  NlwBufferBlock_cnt_3_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D_208,
      O => NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_3_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_3_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_3_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_3_MC_REG_IN : X_BUF
    port map (
      I => cnt_3_MC_tsimcreated_xor_Q_209,
      O => NlwBufferSignal_cnt_3_MC_REG_IN
    );
  NlwBufferBlock_cnt_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_3_MC_REG_CLK
    );
  NlwBufferBlock_cnt_3_MC_D_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D1_210,
      O => NlwBufferSignal_cnt_3_MC_D_IN0
    );
  NlwBufferBlock_cnt_3_MC_D_IN1 : X_BUF
    port map (
      I => cnt_3_MC_D2_211,
      O => NlwBufferSignal_cnt_3_MC_D_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_MC_UIM_169,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_170,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_73,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_ff2nd_nLd_cs_74,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_73,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_ff2nd_nLd_cs_74,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => counteri_ff1st_updown_cs(1),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs(1),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => N_PZ_162_79,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_169,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_170,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_3_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_0_212,
      O => NlwBufferSignal_cnt_3_MC_D2_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_1_213,
      O => NlwBufferSignal_cnt_3_MC_D2_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_2_215,
      O => NlwBufferSignal_cnt_3_MC_D2_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_3_216,
      O => NlwBufferSignal_cnt_3_MC_D2_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_4_217,
      O => NlwBufferSignal_cnt_3_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_3_MC_D_219,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_3_MC_D1_220,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_3_MC_D2_221,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_3_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_3_II_UIM_21,
      O => NlwBufferSignal_counteri_valLd_cs_3_MC_D1_IN1
    );
  NlwBufferBlock_cnt_4_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D_223,
      O => NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_4_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_4_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_4_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_4_MC_REG_IN : X_BUF
    port map (
      I => cnt_4_MC_tsimcreated_xor_Q_224,
      O => NlwBufferSignal_cnt_4_MC_REG_IN
    );
  NlwBufferBlock_cnt_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_4_MC_REG_CLK
    );
  NlwBufferBlock_cnt_4_MC_D_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D1_225,
      O => NlwBufferSignal_cnt_4_MC_D_IN0
    );
  NlwBufferBlock_cnt_4_MC_D_IN1 : X_BUF
    port map (
      I => cnt_4_MC_D2_226,
      O => NlwBufferSignal_cnt_4_MC_D_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_4_MC_UIM_172,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => counteri_ff2nd_nLd_cs_74,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_4_MC_UIM_172,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_MC_UIM_169,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_170,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => counteri_ff2nd_nLd_cs_74,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_4_MC_UIM_172,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => counteri_valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => counteri_ff1st_updown_cs(1),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs(1),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => N_PZ_162_79,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_169,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_170,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_4_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_0_227,
      O => NlwBufferSignal_cnt_4_MC_D2_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_1_229,
      O => NlwBufferSignal_cnt_4_MC_D2_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_2_230,
      O => NlwBufferSignal_cnt_4_MC_D2_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_3_231,
      O => NlwBufferSignal_cnt_4_MC_D2_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_4_232,
      O => NlwBufferSignal_cnt_4_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_4_MC_D_234,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_4_MC_D1_235,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_4_MC_D2_236,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_4_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_4_II_UIM_23,
      O => NlwBufferSignal_counteri_valLd_cs_4_MC_D1_IN1
    );
  NlwBufferBlock_cnt_5_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D_238,
      O => NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_5_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_5_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_5_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_5_MC_REG_IN : X_BUF
    port map (
      I => cnt_5_MC_tsimcreated_xor_Q_239,
      O => NlwBufferSignal_cnt_5_MC_REG_IN
    );
  NlwBufferBlock_cnt_5_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_5_MC_REG_CLK
    );
  NlwBufferBlock_cnt_5_MC_D_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D1_240,
      O => NlwBufferSignal_cnt_5_MC_D_IN0
    );
  NlwBufferBlock_cnt_5_MC_D_IN1 : X_BUF
    port map (
      I => cnt_5_MC_D2_241,
      O => NlwBufferSignal_cnt_5_MC_D_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_5_MC_UIM_173,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => counteri_ff2nd_nLd_cs_74,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_5_MC_UIM_173,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => counteri_ff1st_nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => counteri_ff2nd_nLd_cs_74,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_5_MC_UIM_173,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => counteri_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_MC_UIM_169,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_170,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_4_MC_UIM_172,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => counteri_ff1st_updown_cs(1),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs(1),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => N_PZ_162_79,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_169,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_170,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_4_MC_UIM_172,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_5_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_0_242,
      O => NlwBufferSignal_cnt_5_MC_D2_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_1_244,
      O => NlwBufferSignal_cnt_5_MC_D2_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_2_245,
      O => NlwBufferSignal_cnt_5_MC_D2_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_3_246,
      O => NlwBufferSignal_cnt_5_MC_D2_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_4_247,
      O => NlwBufferSignal_cnt_5_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_5_MC_D_249,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_5_MC_D1_250,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_5_MC_D2_251,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_5_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_5_II_UIM_25,
      O => NlwBufferSignal_counteri_valLd_cs_5_MC_D1_IN1
    );
  NlwBufferBlock_cnt_6_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D_253,
      O => NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_6_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_6_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_6_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_6_MC_REG_IN : X_BUF
    port map (
      I => cnt_6_MC_tsimcreated_xor_Q_254,
      O => NlwBufferSignal_cnt_6_MC_REG_IN
    );
  NlwBufferBlock_cnt_6_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_6_MC_REG_CLK
    );
  NlwBufferBlock_cnt_6_MC_D_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D1_255,
      O => NlwBufferSignal_cnt_6_MC_D_IN0
    );
  NlwBufferBlock_cnt_6_MC_D_IN1 : X_BUF
    port map (
      I => cnt_6_MC_D2_256,
      O => NlwBufferSignal_cnt_6_MC_D_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_6_MC_UIM_174,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_6_MC_UIM_174,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_6_MC_UIM_174,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_MC_UIM_169,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_170,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_4_MC_UIM_172,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_5_MC_UIM_173,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => counteri_ff1st_updown_cs(1),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => counteri_ff2nd_updown_cs(1),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => N_PZ_162_79,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_169,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_170,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_4_MC_UIM_172,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_5_MC_UIM_173,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_6_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_0_257,
      O => NlwBufferSignal_cnt_6_MC_D2_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_1_259,
      O => NlwBufferSignal_cnt_6_MC_D2_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_2_260,
      O => NlwBufferSignal_cnt_6_MC_D2_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_3_261,
      O => NlwBufferSignal_cnt_6_MC_D2_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_4_262,
      O => NlwBufferSignal_cnt_6_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_6_MC_D_264,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_6_MC_D1_265,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_6_MC_D2_266,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_6_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_6_II_UIM_27,
      O => NlwBufferSignal_counteri_valLd_cs_6_MC_D1_IN1
    );
  NlwBufferBlock_cnt_8_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D_268,
      O => NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_8_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_8_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_8_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_8_MC_REG_IN : X_BUF
    port map (
      I => cnt_8_MC_tsimcreated_xor_Q_269,
      O => NlwBufferSignal_cnt_8_MC_REG_IN
    );
  NlwBufferBlock_cnt_8_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_8_MC_REG_CLK
    );
  NlwBufferBlock_cnt_8_MC_D_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D1_270,
      O => NlwBufferSignal_cnt_8_MC_D_IN0
    );
  NlwBufferBlock_cnt_8_MC_D_IN1 : X_BUF
    port map (
      I => cnt_8_MC_D2_271,
      O => NlwBufferSignal_cnt_8_MC_D_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_8_MC_UIM_141,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_7_MC_UIM_139,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_140,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_8_MC_UIM_141,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(8),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_7_MC_UIM_139,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_140,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_8_MC_UIM_141,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(8),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_0_272,
      O => NlwBufferSignal_cnt_8_MC_D2_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_1_273,
      O => NlwBufferSignal_cnt_8_MC_D2_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_2_275,
      O => NlwBufferSignal_cnt_8_MC_D2_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_3_276,
      O => NlwBufferSignal_cnt_8_MC_D2_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_4_277,
      O => NlwBufferSignal_cnt_8_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_8_MC_D_279,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_8_MC_D1_280,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_8_MC_D2_281,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_8_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_8_II_UIM_29,
      O => NlwBufferSignal_counteri_valLd_cs_8_MC_D1_IN1
    );
  NlwBufferBlock_cnt_9_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D_283,
      O => NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_9_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_9_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_9_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_9_MC_REG_IN : X_BUF
    port map (
      I => cnt_9_MC_tsimcreated_xor_Q_284,
      O => NlwBufferSignal_cnt_9_MC_REG_IN
    );
  NlwBufferBlock_cnt_9_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_9_MC_REG_CLK
    );
  NlwBufferBlock_cnt_9_MC_D_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D1_285,
      O => NlwBufferSignal_cnt_9_MC_D_IN0
    );
  NlwBufferBlock_cnt_9_MC_D_IN1 : X_BUF
    port map (
      I => cnt_9_MC_D2_286,
      O => NlwBufferSignal_cnt_9_MC_D_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_9_MC_UIM_142,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_9_MC_UIM_142,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_7_MC_UIM_139,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_140,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_8_MC_UIM_141,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_9_MC_UIM_142,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_7_MC_UIM_139,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_140,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_8_MC_UIM_141,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_0_287,
      O => NlwBufferSignal_cnt_9_MC_D2_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_1_289,
      O => NlwBufferSignal_cnt_9_MC_D2_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_2_290,
      O => NlwBufferSignal_cnt_9_MC_D2_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_3_291,
      O => NlwBufferSignal_cnt_9_MC_D2_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_4_292,
      O => NlwBufferSignal_cnt_9_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_9_MC_D_294,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_9_MC_D1_295,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_9_MC_D2_296,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_9_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_9_II_UIM_31,
      O => NlwBufferSignal_counteri_valLd_cs_9_MC_D1_IN1
    );
  NlwBufferBlock_cnt_11_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D_299,
      O => NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_11_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_11_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_11_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_11_MC_REG_IN : X_BUF
    port map (
      I => cnt_11_MC_tsimcreated_xor_Q_300,
      O => NlwBufferSignal_cnt_11_MC_REG_IN
    );
  NlwBufferBlock_cnt_11_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_11_MC_REG_CLK
    );
  NlwBufferBlock_cnt_11_MC_D_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D1_301,
      O => NlwBufferSignal_cnt_11_MC_D_IN0
    );
  NlwBufferBlock_cnt_11_MC_D_IN1 : X_BUF
    port map (
      I => cnt_11_MC_D2_302,
      O => NlwBufferSignal_cnt_11_MC_D_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_11_MC_UIM_298,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_11_MC_UIM_298,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_11_MC_UIM_298,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_10_MC_UIM_130,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_7_MC_UIM_139,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_140,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_8_MC_UIM_141,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_9_MC_UIM_142,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_10_MC_UIM_130,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_7_MC_UIM_139,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_140,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_8_MC_UIM_141,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_9_MC_UIM_142,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_11_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_0_303,
      O => NlwBufferSignal_cnt_11_MC_D2_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_1_305,
      O => NlwBufferSignal_cnt_11_MC_D2_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_2_306,
      O => NlwBufferSignal_cnt_11_MC_D2_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_3_307,
      O => NlwBufferSignal_cnt_11_MC_D2_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_4_308,
      O => NlwBufferSignal_cnt_11_MC_D2_IN4
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_REG_IN : X_BUF
    port map (
      I => counteri_valLd_cs_11_MC_D_310,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_REG_IN
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_REG_CLK
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_D_IN0 : X_BUF
    port map (
      I => counteri_valLd_cs_11_MC_D1_311,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_D_IN1 : X_BUF
    port map (
      I => counteri_valLd_cs_11_MC_D2_312,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_D_IN1
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN0
    );
  NlwBufferBlock_counteri_valLd_cs_11_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_11_II_UIM_33,
      O => NlwBufferSignal_counteri_valLd_cs_11_MC_D1_IN1
    );
  NlwBufferBlock_err_MC_REG_IN : X_BUF
    port map (
      I => err_MC_D_315,
      O => NlwBufferSignal_err_MC_REG_IN
    );
  NlwBufferBlock_err_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_err_MC_REG_CLK
    );
  NlwBufferBlock_err_MC_D_IN0 : X_BUF
    port map (
      I => err_MC_D1_316,
      O => NlwBufferSignal_err_MC_D_IN0
    );
  NlwBufferBlock_err_MC_D_IN1 : X_BUF
    port map (
      I => err_MC_D2_317,
      O => NlwBufferSignal_err_MC_D_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => counteri_ff1st_nClr_cs_318,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => err_MC_UIM_314,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => err_MC_UIM_314,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => counteri_ff2nd_nClr_cs_320,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_10_MC_UIM_130,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_7_MC_UIM_139,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_140,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_8_MC_UIM_141,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_9_MC_UIM_142,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => cnt_11_MC_UIM_298,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN7 : X_BUF
    port map (
      I => counteri_ff1st_nClr_cs_318,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN7
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_10_MC_UIM_130,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_7_MC_UIM_139,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_140,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_8_MC_UIM_141,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_9_MC_UIM_142,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_11_MC_UIM_298,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => counteri_ff2nd_nClr_cs_320,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_10_MC_UIM_130,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_7_MC_UIM_139,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_140,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_8_MC_UIM_141,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_9_MC_UIM_142,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_11_MC_UIM_298,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => counteri_ff1st_nClr_cs_318,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => N_PZ_139_70,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => N_PZ_136_80,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => cnt_10_MC_UIM_130,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => cnt_7_MC_UIM_139,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => counteri_Madd_result_v_add0000_or0005_140,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN5 : X_BUF
    port map (
      I => cnt_8_MC_UIM_141,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN6 : X_BUF
    port map (
      I => cnt_9_MC_UIM_142,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN7 : X_BUF
    port map (
      I => cnt_11_MC_UIM_298,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN7
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN8 : X_BUF
    port map (
      I => counteri_ff2nd_nClr_cs_320,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN8
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN9 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN9
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN10 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN10
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN11 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN11
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN12 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN12
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN13 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN13
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN14 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN14
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN15 : X_BUF
    port map (
      I => Vcc_67,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN15
    );
  NlwBufferBlock_err_MC_D2_IN0 : X_BUF
    port map (
      I => err_MC_D2_PT_0_319,
      O => NlwBufferSignal_err_MC_D2_IN0
    );
  NlwBufferBlock_err_MC_D2_IN1 : X_BUF
    port map (
      I => err_MC_D2_PT_1_321,
      O => NlwBufferSignal_err_MC_D2_IN1
    );
  NlwBufferBlock_err_MC_D2_IN2 : X_BUF
    port map (
      I => err_MC_D2_PT_2_322,
      O => NlwBufferSignal_err_MC_D2_IN2
    );
  NlwBufferBlock_err_MC_D2_IN3 : X_BUF
    port map (
      I => err_MC_D2_PT_3_323,
      O => NlwBufferSignal_err_MC_D2_IN3
    );
  NlwBufferBlock_err_MC_D2_IN4 : X_BUF
    port map (
      I => err_MC_D2_PT_4_324,
      O => NlwBufferSignal_err_MC_D2_IN4
    );
  NlwBufferBlock_err_MC_D2_IN5 : X_BUF
    port map (
      I => err_MC_D2_PT_5_325,
      O => NlwBufferSignal_err_MC_D2_IN5
    );
  NlwBufferBlock_counteri_ff1st_nClr_cs_MC_REG_IN : X_BUF
    port map (
      I => counteri_ff1st_nClr_cs_MC_D_327,
      O => NlwBufferSignal_counteri_ff1st_nClr_cs_MC_REG_IN
    );
  NlwBufferBlock_counteri_ff1st_nClr_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_ff1st_nClr_cs_MC_REG_CLK
    );
  NlwBufferBlock_counteri_ff1st_nClr_cs_MC_D_IN0 : X_BUF
    port map (
      I => counteri_ff1st_nClr_cs_MC_D1_328,
      O => NlwBufferSignal_counteri_ff1st_nClr_cs_MC_D_IN0
    );
  NlwBufferBlock_counteri_ff1st_nClr_cs_MC_D_IN1 : X_BUF
    port map (
      I => counteri_ff1st_nClr_cs_MC_D2_329,
      O => NlwBufferSignal_counteri_ff1st_nClr_cs_MC_D_IN1
    );
  NlwBufferBlock_counteri_ff1st_nClr_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_ff1st_nClr_cs_MC_D1_IN0
    );
  NlwBufferBlock_counteri_ff1st_nClr_cs_MC_D1_IN1 : X_BUF
    port map (
      I => nClr_II_UIM_35,
      O => NlwBufferSignal_counteri_ff1st_nClr_cs_MC_D1_IN1
    );
  NlwBufferBlock_counteri_ff2nd_nClr_cs_MC_REG_IN : X_BUF
    port map (
      I => counteri_ff2nd_nClr_cs_MC_D_331,
      O => NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_REG_IN
    );
  NlwBufferBlock_counteri_ff2nd_nClr_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_REG_CLK
    );
  NlwBufferBlock_counteri_ff2nd_nClr_cs_MC_D_IN0 : X_BUF
    port map (
      I => counteri_ff2nd_nClr_cs_MC_D1_332,
      O => NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_D_IN0
    );
  NlwBufferBlock_counteri_ff2nd_nClr_cs_MC_D_IN1 : X_BUF
    port map (
      I => counteri_ff2nd_nClr_cs_MC_D2_333,
      O => NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_D_IN1
    );
  NlwBufferBlock_counteri_ff2nd_nClr_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_D1_IN0
    );
  NlwBufferBlock_counteri_ff2nd_nClr_cs_MC_D1_IN1 : X_BUF
    port map (
      I => counteri_ff1st_nClr_cs_318,
      O => NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_D1_IN1
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_counteri_ff1st_nLd_cs_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_counteri_ff1st_nLd_cs_MC_D_IN0,
      O => NlwInverterSignal_counteri_ff1st_nLd_cs_MC_D_IN0
    );
  NlwInverterBlock_counteri_ff1st_nLd_cs_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_counteri_ff1st_nLd_cs_MC_D1_IN1,
      O => NlwInverterSignal_counteri_ff1st_nLd_cs_MC_D1_IN1
    );
  NlwInverterBlock_counteri_ff2nd_nLd_cs_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_D_IN0,
      O => NlwInverterSignal_counteri_ff2nd_nLd_cs_MC_D_IN0
    );
  NlwInverterBlock_counteri_ff2nd_nLd_cs_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_counteri_ff2nd_nLd_cs_MC_D1_IN1,
      O => NlwInverterSignal_counteri_ff2nd_nLd_cs_MC_D1_IN1
    );
  NlwInverterBlock_N_PZ_162_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_162_MC_D1_IN1,
      O => NlwInverterSignal_N_PZ_162_MC_D1_IN1
    );
  NlwInverterBlock_N_PZ_136_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_136_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_N_PZ_136_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_N_PZ_139_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_139_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_N_PZ_139_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_4_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN0,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN0
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_3_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN0,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_3_IN0
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_counteri_Madd_result_v_add0000_or0005_MC_D1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D1_IN0,
      O => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D1_IN0
    );
  NlwInverterBlock_counteri_Madd_result_v_add0000_or0005_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D1_IN1,
      O => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D1_IN1
    );
  NlwInverterBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN5 : X_INV
    port map (
      I => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN5,
      O => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN5
    );
  NlwInverterBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN6 : X_INV
    port map (
      I => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN6,
      O => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN6
    );
  NlwInverterBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN7 : X_INV
    port map (
      I => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN7,
      O => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_1_IN7
    );
  NlwInverterBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_counteri_Madd_result_v_add0000_or0005_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_3_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN0,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_3_IN0
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN0,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN0
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_4_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN0
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_err_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN5 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN5,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN5
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN6 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN6,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN6
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_err_MC_D2_PT_4_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_4_IN0,
      O => NlwInverterSignal_err_MC_D2_PT_4_IN0
    );
  NlwInverterBlock_err_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_err_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_err_MC_D2_PT_5_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_5_IN0,
      O => NlwInverterSignal_err_MC_D2_PT_5_IN0
    );
  NlwInverterBlock_err_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_err_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_err_MC_D2_PT_5_IN8 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_5_IN8,
      O => NlwInverterSignal_err_MC_D2_PT_5_IN8
    );
  NlwInverterBlock_counteri_ff1st_nClr_cs_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_counteri_ff1st_nClr_cs_MC_D_IN0,
      O => NlwInverterSignal_counteri_ff1st_nClr_cs_MC_D_IN0
    );
  NlwInverterBlock_counteri_ff1st_nClr_cs_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_counteri_ff1st_nClr_cs_MC_D1_IN1,
      O => NlwInverterSignal_counteri_ff1st_nClr_cs_MC_D1_IN1
    );
  NlwInverterBlock_counteri_ff2nd_nClr_cs_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_D_IN0,
      O => NlwInverterSignal_counteri_ff2nd_nClr_cs_MC_D_IN0
    );
  NlwInverterBlock_counteri_ff2nd_nClr_cs_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_counteri_ff2nd_nClr_cs_MC_D1_IN1,
      O => NlwInverterSignal_counteri_ff2nd_nClr_cs_MC_D1_IN1
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => PRLD);

end Structure;

