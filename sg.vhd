-- stimuli generator for 12bit counter
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A4



library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.std_logic_unsigned.all;
    
library std;
    use std.textio.all;
    
entity sg is
    port (
        up    : out  std_logic                     := '0';
        down  : out  std_logic                     := '0';
        valLd : out  std_logic_vector(11 downto 0) := (others=>'0');
        nLd   : out  std_logic                     := '1';
        nClr  : out  std_logic                     := '1';
        --
        clk  : out std_logic;
        nres : out std_logic
    );
end entity sg;

architecture arc of sg is

-- Das Arbeiten mit 1/8 Takt-Zyklen ist eigentlich "uebertrieben",
    --   aber es erleichtert das Lesen der Waves
    --   und es bleibt auf den Stimuli Generator beschraenkt
    -- 8*2500ps = 20ns Takt-Periode => 50MHz Takt-Frequenz
    constant oneEigthClockCycle     : time  := 2500 ps;
    constant sevenEigthClockCycle   : time  := 7 * oneEigthClockCycle;
    constant halfClockCycle         : time  := 4 * oneEigthClockCycle;
    constant fullClockCycle         : time  := 8 * oneEigthClockCycle;
    
    signal   simulationRunning_s    : boolean  := true;     -- for internal signalling that simulation is running 
    signal   VDD                    : std_logic  := '1';    -- always one
    signal   GND                    : std_logic  := '0';    -- always zero
    signal   nres_s                 : std_logic  := 'L';
    signal   clk_s                  : std_logic;
begin

    VDD <= '1';
    GND <= '0';
    clk <= clk_s;
    nres <= nres_s;
    ----------------------------------------------------------------
    -- Der folgende PROCESS markiert im Transcript-Fenster das Ende des RESETs
    -- In der Konsequenz muessen dann alle Signale initialisiert und "wohl-definiert" sein
    markStart:                                              -- mark that REST has passed and hence, all signals have to be valid
    process is
        variable wlb : line;
    begin
        -- wait for RESET to appear
        if  nres_s/='0'  then  wait until nres_s='0';  end if;
        -- (low active) RESET is active
        --
        -- wait for RESET to vanish
        wait until nres_s='1';
        -- RESET has passed
        
        -- mark that all signals have to be "valid"
        write( wlb, string'( "###############################################################################" ) );     writeline( output, wlb );
        write( wlb, string'( "###############################################################################" ) );     writeline( output, wlb );
        write( wlb, string'( "###############################################################################" ) );     writeline( output, wlb );
        write( wlb, string'( "###" ) );                                                                                 writeline( output, wlb );
        write( wlb, string'( "###" ) );                                                                                 writeline( output, wlb );
        write( wlb, string'( "###   RESET has passed - all signals have to be valid  @ " ) );
        write( wlb, now );                                                                                              writeline( output, wlb );
        write( wlb, string'( "###" ) );                                                                                 writeline( output, wlb );
        
        wait;
    end process markStart;

    
    clk_gen:
    process is
    begin
        while simulationRunning_s = true loop
            clk_s <= '1';
            --clk   <= '1';
            wait for halfClockCycle;
            clk_s <= '0';
            --clk   <= '0';
            wait for halfClockCycle;
        end loop;
        wait;
    end process clk_gen;
    
    sg:
    process is
    begin   
        simulationRunning_s <= true;    -- a switch to stop simulation, so that there will be only one runtime-cycle

        ---------------------------------------------
        -- do a reset:
        nres_s <= '0';                                      -- set low active reset
        for i in 1 to 2 loop
            wait until '1'=clk_s and clk_s'event;           -- wait for rising clock edge
        end loop;
        -- since 2 rising clk edges have passed, synchronous reset must have been executed
        
        wait for oneEigthClockCycle;
        nres_s <= '1';                                      -- clear low active reset
        ---------------------------------------------
        
        
        -- give CPLD some time to wake up
        for  i in 1 to 10 loop
            wait until '1'=clk_s and clk_s'event;       -- wait for rising clock edge
        end loop;
        -- CPLD is configured and able to react
        
        
        --up: counting up 5 times
        up    <= '1';
		wait for fullClockCycle;
        up    <= '0';
        wait for fullClockCycle;
        up    <= '1';
		wait for fullClockCycle;
        up    <= '0';
        wait for fullClockCycle;
        up    <= '1';
		wait for fullClockCycle * 8;
        up    <= '0';
        wait for fullClockCycle;
        up    <= '1';
		wait for fullClockCycle;
        up    <= '0';
        wait for fullClockCycle;
        up    <= '1';
		wait for fullClockCycle;
        up    <= '0';
        wait for fullClockCycle;
        
        wait for fullClockCycle;
        
        ---------------------------------------------
        -- do a reset:
        nres_s <= '0';                                      -- set low active reset
        for i in 1 to 2 loop
            wait until '1'=clk_s and clk_s'event;           -- wait for rising clock edge
        end loop;
        -- since 2 rising clk edges have passed, synchronous reset must have been executed
        
        wait for oneEigthClockCycle;
        nres_s <= '1';                                      -- clear low active reset
        ---------------------------------------------
        
        
        --load
        valLd <= "000000010000"; -- set value to 16
        nLd   <= '0';
        wait for fullClockCycle;
        nLd   <= '1';
        wait for fullClockCycle * 2;
        
        
        --down: counting down 10 times
        for i in 1 to 4 loop
            down  <= '1';
            wait for fullClockCycle;
            down  <= '0';
            wait for fullClockCycle;
        end loop;
        
        down  <= '1';
        wait for fullClockCycle * 8;
        down  <= '0';
        wait for fullClockCycle;
            
        for i in 1 to 5 loop
        down  <= '1';
        wait for fullClockCycle;
        down  <= '0';
        wait for fullClockCycle;
        end loop;
            
        wait for fullClockCycle * 2;
        
        
        --overflow test
        valLd <= (others=>'1'); -- load max value
        nLd   <= '0';
        wait for fullClockCycle;
        nLd   <= '1';
        -- loading value finisched
		
		wait for fullClockCycle * 5;
        
        --try to incremenent
        up     <= '1';
        wait for fullClockCycle; 
        up    <= '0';

        wait for fullClockCycle * 5; 
        
        
        
        nClr <= '0';
        wait for fullClockCycle; 
        nClr <= '1';

        wait for fullClockCycle * 2; 
        
        
        
        
        ---------------------------------------------
        -- do a reset:
        nres_s <= '0';                                      -- set low active reset
        for i in 1 to 2 loop
            wait until '1'=clk_s and clk_s'event;           -- wait for rising clock edge
        end loop;
        -- since 2 rising clk edges have passed, synchronous reset must have been executed
        
        wait for oneEigthClockCycle;
        nres_s <= '1';                                      -- clear low active reset
        ---------------------------------------------
        
        
        
        --try to decrement
        down <= '1';
        wait for fullClockCycle;
        down <= '0';
        
        wait for fullClockCycle * 4;
        
        nClr <= '0';
        wait for fullClockCycle; 
        nClr <= '1';
        
        wait for fullClockCycle * 4;
        simulationRunning_s <= false;                   -- stop simulation
        --
        wait;
    end process sg;
end architecture arc;