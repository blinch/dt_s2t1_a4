-- up/down counter for 12bit value range
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A4


library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;

entity counter is
    port (
        err     : out std_logic;
        cnt     : out std_logic_vector( 11 downto 0);
        --
        valLd   : in  std_logic_vector( 11 downto 0);
        nLd     : in  std_logic;
        nClr    : in  std_logic;
        up      : in  std_logic;
        down    : in  std_logic;
        --
        clk     : in  std_logic;
        nres    : in  std_logic
    );
end entity counter;